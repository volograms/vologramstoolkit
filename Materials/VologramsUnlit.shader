﻿Shader "Volograms/Unlit" {
    Properties {
        _Color ("Main Color", Color) = (1,1,1,1)
        _VMainTex ("Base (RGB)", 2D) = "white" {}
        _ColorIntensity ("Color Intensity", Float) = 1
    }

    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
            #pragma surface surf NoLighting

            sampler2D _VMainTex;
            fixed4 _Color;
            float _ColorIntensity;

            struct Input {
                float2 uv_VMainTex;
                float3 worldPos;
            };
            
            fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
            {
                fixed4 c;
                c.rgb = s.Albedo; 
                c.a = s.Alpha;
                return c;
            }

            void surf (Input IN, inout SurfaceOutput o) {
                fixed4 c = tex2D(_VMainTex, IN.uv_VMainTex) * _Color;
                o.Albedo = c.rgb * _ColorIntensity;
                o.Alpha = c.a;
            }
        ENDCG
    }

    Fallback "Legacy Shaders/Diffuse"
}


﻿using System.Collections;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using ProgressFunc = System.Action<float>;
using CompleteFunc = System.Action<byte[]>;
using FailureFunc = System.Action<string>;

public class VologramDownloader : MonoBehaviour
{
    public string downloadLocation = "Downloaded";

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void Download(string url)
    {
    }

    private void DownloadNextFrame(string url, ProgressFunc progressFunc = null,
        CompleteFunc completeFunc = null, FailureFunc failureFunc = null)
    {
        WebClient webClient = new WebClient();
    }

    private IEnumerator Download_(string url, ProgressFunc progressFunc = null,
        CompleteFunc completeFunc = null, FailureFunc failureFunc = null)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            if (progressFunc != null)
            {
                webRequest.SendWebRequest();

                while (!webRequest.isDone)
                {
                    progressFunc(webRequest.downloadProgress);
                    yield return 0;
                }
            }
            else
            {
                yield return webRequest.SendWebRequest();
            }

            if (webRequest.isNetworkError)
                failureFunc?.Invoke(webRequest.error);
            else
                completeFunc?.Invoke(webRequest.downloadHandler.data);
        }
    }

    private IEnumerator DownloadAudio_(string url, ProgressFunc progressFunc = null,
        CompleteFunc completeFunc = null, FailureFunc failureFunc = null)
    {
        using (UnityWebRequest webRequest = UnityWebRequestMultimedia.GetAudioClip(url, AudioType.MPEG))
        {
            if (progressFunc != null)
            {
                webRequest.SendWebRequest();

                while (!webRequest.isDone)
                {
                    progressFunc(webRequest.downloadProgress);
                    yield return 0;
                }
            }
            else
            {
                yield return webRequest.SendWebRequest();
            }

            if (webRequest.isNetworkError)
                failureFunc?.Invoke(webRequest.error);
            else
                completeFunc?.Invoke(webRequest.downloadHandler.data);
        }
    }
}

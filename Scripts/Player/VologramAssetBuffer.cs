﻿// Copyright (C) 2018 Volograms Limited. All rights reserved.
//
// This file is part of Volograms Toolkit SDK.
//
// See LICENSE in the project root for license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Profiling;
using VologramsToolkit.Scripts.Serialization;


/// <summary>
///     Class for buffering vologram frame data
/// </summary>
public class VologramAssetBuffer
{
    #region VARIABLES

    public VologramAssetLoader Loader { get; }
    private readonly int _startFrame;
    private readonly int _endFrame;

    private readonly Queue<KeyValuePair<int, VologramMesh[]>> _bufferedMeshes;
    private KeyValuePair<int, VologramMesh[]> _framePair;
    private VologramMesh _lostKeyFrame;

    private int _lastDequeuedId = -1;
    private int _lastEnqueuedId = -1;
    private int _nextEnqueueFrameId;

    // Create a new Mutex. The creating thread does not own the mutex.
    private static readonly Mutex Mut = new Mutex();

    // reverse animation at the end
    private bool _boomerangLoop;
    private bool _countUp = true;

    private bool _bufferingPaused = false;

    #endregion

    #region PROPERTIES

    /// <summary>
    ///     Gets the number of frames currently buffered.
    /// </summary>
    /// <value>The buffered meshes.</value>
    public int BufferedFrames => _bufferedMeshes.Count;

    /// <summary>
    ///     Gets a value indicating whether this <see cref="T:VOLAssetBuffer" /> is fully buffered.
    /// </summary>
    /// <value><c>true</c> if is buffered; otherwise, <c>false</c>.</value>
    public bool IsBuffered => _bufferedMeshes.Count >= BufferLength;

    public bool Buffering { get; private set; }

    /// <summary>
    ///     Gets the length of the buffer.
    /// </summary>
    /// <value>The length of the buffer.</value>
    public int BufferLength { get; }

    #endregion

    #region INITIALISATION

    /// <summary>
    ///     Initialize the asset buffer.
    /// </summary>
    /// <param name="bufferLength">Buffer length</param>
    /// <param name="loader">Asset loader</param>
    /// <param name="start">First frame to be buffered</param>
    /// <param name="end">Last frame to be buffered</param>
    public VologramAssetBuffer(int bufferLength, VologramAssetLoader loader, int start, int end, bool boomerang = false)
    {
        if (bufferLength <= 0)
            throw new ArgumentException("Value cannot be negative or zero", nameof(bufferLength));

        if (start < 0)
            throw new ArgumentException("Value cannot be negative", nameof(start));

        if (end < 0)
            throw new ArgumentException("Value cannot be negative", nameof(end));

        if (start > end)
            throw new ArgumentException("Value of " + nameof(start) + " must be greater than the value of " +
                                        nameof(end));

        Loader = loader ?? throw new ArgumentNullException(nameof(loader));

        BufferLength = bufferLength;

        _startFrame = start;
        _endFrame = Mathf.Min(end, loader.FrameCount - 1);

        _bufferedMeshes = new Queue<KeyValuePair<int, VologramMesh[]>>();

        VologramMesh[] firstVolsMesh = Loader.LoadedMeshes;
        _bufferedMeshes.Enqueue(new KeyValuePair<int, VologramMesh[]>(firstVolsMesh[0].Id, firstVolsMesh));
        _lastEnqueuedId = firstVolsMesh[0].Id;
        _nextEnqueueFrameId = _lastEnqueuedId + 1;

        _boomerangLoop = Loader.Version >= 12 && boomerang;
    }

    #endregion

    #region MAIN_LOGIC

    /// <summary>
    ///     Starts the buffering.
    /// </summary>
    public void StartBuffering()
    {
        if (Buffering) return;

        Thread bufferThread = new Thread(BufferLoaderAsync)
        {
            IsBackground = true, 
            Name = "Buffering"
        };
        Buffering = true;
        bufferThread.Start();
    }

    public void StopBuffering()
    {
        Buffering = false;
    }

    private void BufferLoaderAsync()
    {
        //Debug.Log("Start BufferLoader Thread!\n");
        while (Buffering)
            try
            {
#if UNITY_EDITOR
                Profiler.BeginThreadProfiling("Volograms Threads", "Buffer Loader");
#endif
                if (_bufferedMeshes.Count < BufferLength && !_bufferingPaused)
                {
                    if (_boomerangLoop)
                        PrepareNextFrameBoomerang();
                    else
                        PrepareNextFrame();
                }
                else
                {
                    Thread.Sleep(5);
                }
#if UNITY_EDITOR
                Profiler.EndThreadProfiling();
#endif
                Thread.Sleep(10);
            }
            catch
            {
                Buffering = false;
                throw;
            }
    }

    public IEnumerator StartBufferingAndroid()
    {
        if (Buffering) yield break;
        Buffering = true;

        while (Buffering)
        {
            if (_bufferedMeshes.Count < BufferLength && _bufferingPaused)
            {
                try
                {
                    if (_boomerangLoop)
                        PrepareNextFrameBoomerang();
                    else
                        PrepareNextFrame();
                }
                catch
                {
                    Buffering = false;
                    throw;
                }
            }
            else
            {
                yield return new WaitForSeconds(0.005f);
            }
        }
    }


    private void PrepareNextFrame()
    {
        VologramMesh[] meshes;
        if (_nextEnqueueFrameId == _lastEnqueuedId + 1 ||
            _nextEnqueueFrameId == 0 && _lastEnqueuedId == Loader.FrameCount - 1)
            meshes = LoadMeshUnique();
        else
            meshes = LoadMeshUnique(_nextEnqueueFrameId);

        if (meshes == null || meshes[0] == null)
            return;

        int meshNumber = meshes[0].Id;
        _bufferedMeshes.Enqueue(new KeyValuePair<int, VologramMesh[]>(meshNumber, meshes));

        _lastEnqueuedId = meshNumber;
        _nextEnqueueFrameId = _lastEnqueuedId + 1;
        if (_nextEnqueueFrameId > _endFrame)
            _nextEnqueueFrameId = _startFrame;
    }

    private void PrepareNextFrameBoomerang()
    {
        VologramMesh[] meshes = _countUp ? LoadMeshUnique() : LoadMeshUnique(_nextEnqueueFrameId);

        _bufferedMeshes.Enqueue(new KeyValuePair<int, VologramMesh[]>(meshes[0].Id, meshes));
        _lastEnqueuedId = meshes[0].Id;

        if (_countUp)
        {
            if (meshes[0].Id >= _endFrame)
            {
                _countUp = false;
                _nextEnqueueFrameId = _lastEnqueuedId - 1;
            }
            else
            {
                _nextEnqueueFrameId++;
            }
        }
        else
        {
            if (meshes[0].Id <= _startFrame)
            {
                _countUp = true;
                _nextEnqueueFrameId = _lastEnqueuedId + 1;
            }
            else
            {
                _nextEnqueueFrameId--;
            }
        }
    }

    private VologramMesh[] LoadMeshUnique(int id = -1)
    {
        Mut.WaitOne();

        VologramMesh[] meshes = id == -1 ? Loader.LoadNextMesh() : Loader.LoadFrameNumber(id);

        Mut.ReleaseMutex();

        return meshes;
    }

    #endregion

    #region RETRIEVAL

    /// <summary>
    ///     Retrieve the desired the frame or the last remaining frame in the buffer.
    /// </summary>
    /// <returns>Frame mesh data</returns>
    /// <param name="requestedFrameNumber">Requested frame number</param>
    public VologramMesh[] RetrieveFrameMesh(int requestedFrameNumber)
    {
        if (_lastDequeuedId == requestedFrameNumber)
            return _framePair.Value;

        if (requestedFrameNumber < _startFrame || requestedFrameNumber > _endFrame)
        {
            Debug.LogError("Requested frame number is not within the correct range: " + _startFrame + " >= " + requestedFrameNumber + " <= " + _endFrame);
            return null;
        }

        _bufferingPaused = true;

        // Look for the desired frame in the buffer
        if (_bufferedMeshes.Count > 0)
        {
            // Dequeue until frame found or the buffer is empty
            do
            {
                _framePair = _bufferedMeshes.Dequeue();

                if (_framePair.Value[0].KeyFrame && _bufferedMeshes.Count > 0 && _framePair.Key != requestedFrameNumber)
                    // Keep track if a dequeued frame was a keyframe
                    // We may need the keyframe data for later
                    _lostKeyFrame = _framePair.Value[0];
            } while (_bufferedMeshes.Count > 0 && _framePair.Key != requestedFrameNumber);

            // if the frame was not found in the buffer, load a new one
            if (_framePair.Key != requestedFrameNumber)
            {
                _nextEnqueueFrameId = requestedFrameNumber + 1;
                VologramMesh[] loadedMeshes = LoadMeshUnique(requestedFrameNumber);
                _framePair = new KeyValuePair<int, VologramMesh[]>(loadedMeshes[0].Id, loadedMeshes);
            }

            // track the number of the loaded frame
            _lastDequeuedId = _framePair.Key;

            // fill in the missing keyframe data if needed
            VologramMesh mesh = _framePair.Value[0];
            if (!mesh.KeyFrame && _lostKeyFrame != null)
            {
                if (_lostKeyFrame.UsingShortIndices)
                    mesh.AddKeyFrameData(_lostKeyFrame.Uvs, _lostKeyFrame.IndicesS);
                else
                    mesh.AddKeyFrameData(_lostKeyFrame.Uvs, _lostKeyFrame.Indices);
            }
            _lostKeyFrame = null;

            _bufferingPaused = false;
            
            return _framePair.Value;
        }

        Debug.LogWarning("Buffer is empty, cannot retrieve");
        return null;
    }

    #endregion

    #region UTIL

    /// <summary>
    ///     Resets the buffer.
    /// </summary>
    public void ResetBuffer(string newLocation)
    {
        _lastDequeuedId = -1;
        _nextEnqueueFrameId = _startFrame;
        // destroy all buffered frames
        _bufferedMeshes.Clear();
        Loader.Reset(newLocation);

        // start loading first frame
        if (_bufferedMeshes.Count < BufferLength)
            PrepareNextFrame();

        _countUp = true;
    }

    public void SetBoomerangLoop(bool flag)
    {
        _boomerangLoop = flag;
        _countUp = true;
    }

    #endregion

    #region PREVIEWING

    /// <summary>
    ///     Peeks the first frame mesh.
    /// </summary>
    /// <returns>The first frame mesh</returns>
    public VologramMesh[] PeekNextFrame()
    {
        return _bufferedMeshes.Count > 0 ? _bufferedMeshes.Peek().Value : null;
    }

    #endregion
}


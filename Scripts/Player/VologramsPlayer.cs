﻿// Copyright (C) 2018 Volograms Limited. All rights reserved.
//
// This file is part of Volograms Toolkit SDK.
//
// See LICENSE in the project root for license information.

using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Video;
using VologramsToolkit.Scripts.Serialization;
using NLayer;

[DisallowMultipleComponent]
[RequireComponent(typeof(VideoPlayer), typeof(AudioSource))]
public class VologramsPlayer : MonoBehaviour
{
    #region CONSTANTS
    // The display name of the visible frames game object.
    private const string MeshHolderName = "Mesh";

    // If the difference between the current time of the audio 
    // and the current frame of the vologram exceeds this 
    // threshold, the two are synced.
    private const float AudioSyncThreshold = 0.50f;
    #endregion
    
    #region ENUMS
    public enum State
    {
        Preparing = 0, 
        Stopped = 1, 
        Paused = 2, 
        Playing = 3,
    }

    public enum PathType
    {
        Absolute,
        StreamingAssets,
        PersistentData
    }

    [Serializable]
    public struct VologramPath
    {
        [SerializeField]
        private PathType pathType;
        [SerializeField]
        private string path;

        public PathType PathType => pathType;
        public string Path => path;
        
        public VologramPath(PathType pathType, string path)
        {
            this.pathType = pathType;
            this.path = path;
        }

        public static VologramPath InterpretFromPath(string path)
        {
            if (path.StartsWith(Application.streamingAssetsPath))
            {
                return new VologramPath(PathType.StreamingAssets,
                    path.Remove(0, Application.streamingAssetsPath.Length + 1));
            }

            if (path.StartsWith(Application.persistentDataPath))
            {
                return new VologramPath(PathType.PersistentData,
                    path.Remove(0, Application.persistentDataPath.Length + 1));
            }

            return new VologramPath(PathType.Absolute, path);
        }

        public string PathTypeAsString 
        {
            get
            {
                switch (PathType)
                {
                    case PathType.Absolute:
                        return string.Empty;
                    case PathType.StreamingAssets:
                        return Application.streamingAssetsPath;
                    case PathType.PersistentData:
                        return Application.persistentDataPath;
                    default:
                        return string.Empty;
                }
            }
        }

        public string FullPath => System.IO.Path.Combine(PathTypeAsString, Path);

        public override string ToString()
        {
            return $"{{{pathType.ToString()}}}/{path}";
        }
    }

    #endregion

    #region PRIVATE_VARIABLES

    // private VologramAssetLoader _assetLoader;
    private VologramAssetBuffer _assetBuffer;

    private GameObject _mediaObject;

    public VideoPlayer VideoTexturePlayer => GetComponent<VideoPlayer>();

    public AudioSource AudioSource => GetComponent<AudioSource>();

    public bool IsPlaying => PlayerState == State.Playing;

    // Mesh filter of the vologram.
    private MeshFilter _filter;

    private MeshRenderer _renderer;

    // Tracks the number of the frame that will be loaded next.
    private int _frameTracker;

    // Tracks time to ensure the vologram plays at the correct speed (using image textures).
    private float _timeTracker;

    // Tracks if the vologram is playing in the Unity Editor.
    private bool _isPausedInEditor;


    // Distinguishes between old and new formats.
    //private int FileType => _assetLoader?.FileType ?? 0;

    private bool _boomerangForward = true;

    private const string MainTexId = "_VMainTex";

    public VologramsHeader Header { get; private set; } = new VologramsHeader();

    public State PlayerState { get; private set; } = State.Preparing;

    private bool _updateRequired;

    #endregion

    #region PUBLIC_VARIABLES

    /// <summary>
    /// The path to the folder containing the .vols files
    /// </summary>
    [SerializeField]
    private VologramPath _assetPath;

    public VologramPath AssetPath
    {
        get => _assetPath;
        set
        {
            if (PlayerState > State.Stopped)
            {
                Debug.LogError("Cannot set AssetPath. Vologram must be stopped first");
            }
            else
            {
                _assetPath = value;
                LoadHeader();
                _updateRequired = true;
            }
        }
    }
    
    /// <summary>
    /// The maximum number of frames that will be buffered during play.
    /// </summary>
    [SerializeField][Range(0, 10)]
    private int bufferSize = 5;

    public int BufferSize
    {
        get => bufferSize;
        set
        {
            if (PlayerState > State.Stopped)
            {
                Debug.LogError("Cannot set BufferSize. Vologram must be stopped first");
            }
            else
            {
                bufferSize = value;
                _updateRequired = true;
            }
        }
    }

    [SerializeField]
    private VologramPath _videoTextureUrl;
    /// <summary>
    /// The video texture file.
    /// </summary>
    public VologramPath VideoTextureUrl
    {
        get => _videoTextureUrl;
        set
        {
            if (PlayerState > State.Stopped)
            {
                Debug.LogError("Cannot set VideoTextureUrl. Vologram must be stopped first");
            }
            else
            {
                _updateRequired = true;
                if (string.IsNullOrEmpty(value.Path))
                {
                    VideoTexturePlayer.source = VideoSource.VideoClip;
                    VideoTexturePlayer.clip = null;
                    return;
                }

                _videoTextureUrl = value;
            }
        }
    }
    
    /// <summary>
    /// The material the video texture is mapped onto.
    /// </summary>
    [SerializeField]
    private Material meshMaterial;
    public Material MeshMaterial
    {
        get => meshMaterial;
        set
        {
            meshMaterial = value;
            if (_renderer != null)
                _renderer.material = value;
        }
    }

    // Audio
    /// <summary>
    /// The audio source of the vologram.
    /// </summary>
    public AudioClip VologramAudio
    {
        get => AudioSource.clip;
        set
        {
            if (PlayerState > State.Stopped)
            {
                Debug.LogError("Cannot set VologramAudio. Vologram must be stopped first");
            }
            else
            {
                _updateRequired = true;
                AudioSource.clip = value;
            }
        }
    }

    /// <summary>
    /// Set the audio starting time (in seconds).
    /// </summary>
    [SerializeField]
    private float audioStartTime;
    public float AudioStartTime
    {
        get => audioStartTime;
        set
        {
            if (PlayerState > State.Stopped)
            {
                Debug.LogError("Cannot set AudioStartTime. Vologram must be stopped first");
            }
            else
            {
                _updateRequired = true;
                audioStartTime = value;
            }
        }
    }

    /// <summary>
    /// The node containing the vologram mesh.
    /// </summary>
    private GameObject _meshObject;

    /// <summary>
    /// The first frame of the vologram that plays.
    /// </summary>
    [SerializeField]
    private int startFrame;
    public int StartFrame
    {
        get => startFrame;
        set
        {
            if (PlayerState > State.Stopped)
            {
                Debug.LogError("Cannot set StartFrame. Vologram must be stopped first");
            }
            else
            {
                _updateRequired = true;
                startFrame = value;
            }
        }
    }

    /// <summary>
    /// The last frame of the vologram that plays before stopping/looping.
    /// </summary>
    [SerializeField]
    private int endFrame = -1;
    public int EndFrame
    {
        get => endFrame;
        set
        {
            if (PlayerState > State.Stopped)
            {
                Debug.LogError("Cannot set EndFrame. Vologram must be stopped first");
            }
            else
            {
                _updateRequired = true;
                endFrame = value;
            }
        }
    }

    /// <summary>
    /// The playback speed of the video player when using video textures.
    /// </summary>
    [SerializeField][Range(0.5f, 2.0f)]
    private float playbackSpeed = 1.0f;
    public float PlaybackSpeed
    {
        get => playbackSpeed;
        set
        {
            if (PlayerState > State.Stopped)
            {
                Debug.LogError("Cannot set PlaybackSpeed. Vologram must be stopped first");
            }
            else
            {
                _updateRequired = true;
                playbackSpeed = Mathf.Clamp(value, 0.5f, 2.0f);
            }
        }
    }

    /// <summary>
    /// The frames per second when using image textures.
    /// </summary>
    [SerializeField]
    private int fps = 30;
    public int FPS
    {
        get => fps;
        set
        {
            if (PlayerState > State.Stopped)
            {
                Debug.LogError("Cannot set FPS. Vologram must be stopped first");
            }
            else
            {
                _updateRequired = true;
                fps = value;
            }
        }
    }

    /// <summary>
    /// Activate to automatically play the vologram after 
    /// it finishes preparing.
    /// </summary>
    [SerializeField]
    private bool playOnStart = true;
    public bool PlayOnStart
    {
        get => playOnStart;
        set
        {
            if (PlayerState > State.Stopped)
            {
                Debug.LogError("Cannot set PlayOnStart. Vologram must be stopped first");
            }
            else
            {
                _updateRequired = true;
                playOnStart = value;
            }
        }
    }
    
    /// <summary>
    /// Activate to automatically restart the vologram when 
    /// it finishes playing.
    /// </summary>
    [SerializeField]
    private bool isLooping = true;
    public bool IsLooping
    {
        get => isLooping;
        set
        {
            _updateRequired = true;
            // TODO: Check if the loop flag has to be set somewhere else
            isLooping = value;
        }
    }
    
    //TODO: Check similar to isLooping
    public bool boomerangLoop;

    public bool Muted
    {
        get
        {
            if (IsAudioEnabled)
                return AudioSource.mute;

            if (IsVideoEnabled)
                return VideoTexturePlayer.audioOutputMode == VideoAudioOutputMode.None;

            return false;
        }
        set
        {
            if (value)
            {
                if (IsAudioEnabled)
                    AudioSource.mute = true;

                else if (IsVideoEnabled)
                    VideoTexturePlayer.audioOutputMode = VideoAudioOutputMode.None;
            }
            else
            {
                if (IsAudioEnabled)
                    AudioSource.mute = false;

                else if (IsVideoEnabled)
                    VideoTexturePlayer.audioOutputMode = VideoAudioOutputMode.Direct;
            }
        }
    }

    /// <summary>
    /// Activate if the vologram is not centred and requires a 
    /// transform adjustment.
    /// </summary>
    public bool adjustmentRequired;

    /// <summary>
    /// The position adjustment of the vologram.
    /// Only applied if adjustmentRequired is set to true.
    /// </summary>
    public Vector3 positionAdj;

    /// <summary>
    /// The rotation adjustment of the vologram.
    /// Only applied if adjustmentRequired is set to true.
    /// </summary>
    public Vector3 rotationAdj;

    /// <summary>
    /// The scale adjustment of the vologram.
    /// Only applied if adjustmentRequired is set to true.
    /// </summary>
    public Vector3 scaleAdj = Vector3.one;

    public int Frame { get; private set; }
    #endregion

    #region DELEGATES_AND_EVENTS
    public delegate void VologramEventDelegate(VologramsPlayer player);
    
    public event VologramEventDelegate OnVologramPrepared;
    
    public event VologramEventDelegate OnVologramPlay;
    
    public event VologramEventDelegate OnVologramPause;
    
    public event VologramEventDelegate OnVologramStop;
    
    public event VologramEventDelegate OnVologramLoop;
    
    public delegate void VologramFrameEventDelegate(VologramsPlayer player, long frameNumber);

    public event VologramFrameEventDelegate OnVologramSeekComplete;
    
    #endregion

    #region NULL_CHECKERS

    private bool IsVideoEnabled
    {
        get => VideoTexturePlayer.enabled;
        set => VideoTexturePlayer.enabled = value;
    }

    private bool IsAudioEnabled
    {
        get => AudioSource.enabled;
        set => AudioSource.enabled = value;
    }

    #endregion

    #region MONO_BEHAVIOUR_FUNCTIONS

    private void Reset()
    {
        gameObject.transform.hideFlags = HideFlags.HideInInspector;
        VideoTexturePlayer.hideFlags = HideFlags.HideInInspector;
        AudioSource.hideFlags = HideFlags.HideInInspector;
        meshMaterial = new Material(Shader.Find("Volograms/Unlit"));
        Setup();
    }

    private void Awake()
    {
        IsVideoEnabled = !string.IsNullOrEmpty(_videoTextureUrl.Path);
        IsAudioEnabled = VologramAudio != null;
        
        VideoTexturePlayer.audioOutputMode = IsAudioEnabled ? VideoAudioOutputMode.None : VideoAudioOutputMode.Direct;
        
        LoadHeader();
    }

    private void OnEnable()
    {
        PlayerState = State.Preparing;
        Setup();
        Init();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        _assetBuffer?.StopBuffering();
        Stop();
        _assetBuffer = null;
    }

    private void Update()
    {
        if (IsVideoEnabled) return;

        _timeTracker += Time.deltaTime;
        float dt = 1.0f / fps;

        if (!(_timeTracker >= dt)) return;
        _timeTracker -= dt;
        PlayNextFrame();
    }
    
    #if !UNITY_EDITOR
    private void OnApplicationFocus(bool focus)
    {
        if (!focus)
        {
            OnDisable();
        }
    }
    #endif
    #endregion

    #region INITIALISATION_FUNCTIONS

    private void Setup()
    {

#if UNITY_ANDROID
            Application.targetFrameRate = 60;
            BetterStreamingAssets.Initialize();
#endif

        // Mesh renderer and filter init
        _meshObject = transform.Find(MeshHolderName)?.gameObject;
        if (_meshObject == null)
        {
            _meshObject = new GameObject(MeshHolderName);
            _meshObject.transform.SetParent(transform);
        }

        _filter = _meshObject.GetComponent<MeshFilter>();
        if (_filter == null)
            _filter = _meshObject.AddComponent<MeshFilter>();
        _filter.mesh = new Mesh();

        _renderer = _meshObject.GetComponent<MeshRenderer>();
        if (_renderer == null)
            _renderer = _meshObject.AddComponent<MeshRenderer>();
        
        _renderer.material =
            meshMaterial == null ?
                new Material(Shader.Find("Volograms/Unlit")) :
                meshMaterial;
        
        if (boomerangLoop && IsVideoEnabled)
        {
            Debug.LogWarning("Boomerang looping is not compatible with video textures. Turning off boomerang looping.");
            boomerangLoop = false;
        }

        if (boomerangLoop && IsAudioEnabled)
        {
            Debug.LogWarning("Boomerang looping is not compatible with audio. Turning off boomerang looping.");
            boomerangLoop = false;
        }
    }

    /// <summary>
    /// Initialises the assets buffer.
    /// </summary>
    private void InitAssetsBuffer()
    {
        VologramAssetLoader assetLoader = new VologramAssetLoader(_assetPath.FullPath, startFrame);
        if (endFrame == -1 || endFrame == startFrame)
        {
            endFrame = Header.TotalFrameCount - 1;
        }
        _assetBuffer = new VologramAssetBuffer(bufferSize, assetLoader, startFrame, endFrame);
#if UNITY_ANDROID && !UNITY_EDITOR
        StartCoroutine(_assetBuffer.StartBufferingAndroid());
#else
        _assetBuffer.StartBuffering();
#endif
        _meshObject.transform.localPosition = new Vector3(Header.Translation[0], Header.Translation[1], Header.Translation[2]);
        _meshObject.transform.localRotation = new Quaternion(Header.Rotation[0], Header.Rotation[1], Header.Rotation[2], Header.Rotation[3]);
        _meshObject.transform.localScale = Vector3.one * Header.Scale;
        _meshObject.transform.Rotate(new Vector3(0, 0, 180));
    }

    public void Init()
    {
        if (!gameObject.activeInHierarchy || !gameObject.activeSelf || string.IsNullOrEmpty(_assetPath.Path))
            return;

        // Prepare vologram assets and loader
        InitAssetsBuffer();

        if (IsVideoEnabled)
        {
            // if using video texture setup the video player
            InitTextureVideoPlayer();
        }
        else
        {
            StartCoroutine(PrepareCompleted_());
        }
    }

    private void InitTextureVideoPlayer()
    {
        VideoTexturePlayer.source = VideoSource.Url;
        VideoTexturePlayer.url = _videoTextureUrl.FullPath;
        VideoTexturePlayer.playbackSpeed = playbackSpeed;
        VideoTexturePlayer.renderMode = VideoRenderMode.MaterialOverride;
        VideoTexturePlayer.targetMaterialRenderer = _renderer;
        VideoTexturePlayer.targetMaterialProperty = MainTexId;
        VideoTexturePlayer.isLooping = isLooping;
        VideoTexturePlayer.frame = startFrame;
        VideoTexturePlayer.frameReady -= FrameReady;
        VideoTexturePlayer.frameReady += FrameReady;
        VideoTexturePlayer.errorReceived -= ErrorReceived;
        VideoTexturePlayer.errorReceived += ErrorReceived;
        VideoTexturePlayer.prepareCompleted -= PrepareCompleted;
        VideoTexturePlayer.prepareCompleted += PrepareCompleted;
        VideoTexturePlayer.loopPointReached -= LoopCompleted;
        VideoTexturePlayer.loopPointReached += LoopCompleted;
        VideoTexturePlayer.seekCompleted -= SeekComplete;
        VideoTexturePlayer.seekCompleted += SeekComplete;
        VideoTexturePlayer.sendFrameReadyEvents = false;
        VideoTexturePlayer.Prepare();
    }
    #endregion

    #region VIDEO_PLAYER_EVENTS

    private void ErrorReceived(VideoPlayer source, string error)
    {
        print($"Video player error: {error}");
    }
    
    private void PrepareCompleted(VideoPlayer source)
    {
        _updateRequired = false;
        StartCoroutine(PrepareCompleted_(source));
    }

    private IEnumerator PrepareCompleted_(VideoPlayer source)
    {
        OnVologramPrepared?.Invoke(this);
        
        _frameTracker = startFrame;
        yield return new WaitUntil(() => _assetBuffer.IsBuffered);

        if (playOnStart && Application.isPlaying)
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            yield return new WaitForSeconds(0.3f);
#endif
            // Start playback
            Play();
        }
        else
            PlayerState = State.Stopped;
    }

    private void LoopCompleted(VideoPlayer source)
    {
        if (!isLooping)
        {
            Stop();
            return;
        }

        OnVologramLoop?.Invoke(this);

#if UNITY_ANDROID && !UNITY_EDITOR
            StartCoroutine(LoopCompleted_(source));
#else
        if (IsAudioEnabled)
        {
            AudioSource.Stop();
            AudioSource.time = audioStartTime + ((float)startFrame / (float)(Header.TotalFrameCount - 1) * (float)fps);
            AudioSource.Play();
        }

        source.frame = startFrame;
        source.Play();

        _frameTracker = startFrame;
#endif
    }

#if UNITY_ANDROID
        // Used only for Android, adds a delay before restarting
        IEnumerator LoopCompleted_(VideoPlayer source)
        {
            Pause();
            
            if (!isLooping)
                yield break;
            
            yield return new WaitForSeconds(0.5f);
            if (IsAudioEnabled)
            {
                AudioSource.time = audioStartTime + ((float)startFrame / (float)(Header.TotalFrameCount - 1) * (float)fps);
                AudioSource.UnPause();
            }

            source.frame = 0;
            source.Play();

            _frameTracker = startFrame;
            source.frame = startFrame;
        }
#endif

    private void SeekComplete(VideoPlayer source)
    {
        long frameIdx = (long)Math.Round(source.clockTime / (1.0 / fps));
        OnVologramSeekComplete?.Invoke(this, frameIdx);
        
        if (IsAudioEnabled && Mathf.Abs(AudioSource.time - (float)source.time - audioStartTime) > AudioSyncThreshold)
        {
            AudioSource.time = (float)source.time + audioStartTime;
        }
        
        if (!source.isPrepared || _assetBuffer == null) return;
        
        VologramMesh[] meshes = _assetBuffer.RetrieveFrameMesh((int)frameIdx);
        
        ShowFrame(meshes);
        
        if (frameIdx == endFrame)
            LoopCompleted(source);
    }

    private void FrameReady(VideoPlayer source, long frameIdx)
    {
        if (frameIdx > endFrame)
        {
            return;
        }

        if (frameIdx < startFrame)
        {
            return;
        }

        if (AudioSource != null && Mathf.Abs(AudioSource.time - (float)source.time - audioStartTime) > AudioSyncThreshold)
        {
            AudioSource.time = (float)source.time + audioStartTime;
        }

        if (!source.isPrepared || _assetBuffer == null) return;

        VologramMesh[] meshes = _assetBuffer.RetrieveFrameMesh((int)frameIdx);

        ShowFrame(meshes);

        if (frameIdx == endFrame)
            LoopCompleted(source);
    }
    #endregion

    #region NON_VIDEO_PLAYER_EVENTS

    private IEnumerator PrepareCompleted_()
    {
        OnVologramPrepared?.Invoke(this);
        _updateRequired = false;
        _frameTracker = startFrame;
        yield return new WaitUntil(() => _assetBuffer.IsBuffered);

        if (playOnStart && Application.isPlaying)
        {
#if UNITY_ANDROID
            yield return new WaitForSeconds(0.3f);
#endif
            // Start playback
            PlayerState = State.Paused;
            Play();
        }
        else
            PlayerState = State.Stopped;
    }

    private void LoopCompleted()
    {
        OnVologramLoop?.Invoke(this);

#if UNITY_ANDROID && !UNITY_EDITOR
            StartCoroutine(LoopCompleted_());
#else
        if (IsAudioEnabled)
        {
            AudioSource.Stop();
            AudioSource.time = audioStartTime + ((float)startFrame / (float)(Header.TotalFrameCount - 1) * (float)fps);
            AudioSource.Play();
        }

        _frameTracker = startFrame;
#endif
    }

#if UNITY_ANDROID
        // Used only for Android, adds a delay before restarting
        private IEnumerator LoopCompleted_()
        {
            Pause();
            
            if (!isLooping)
                yield break;
            
            yield return new WaitForSeconds(0.5f);
            if (IsAudioEnabled)
            {
                AudioSource.time = audioStartTime + ((float)startFrame / (float)(Header.TotalFrameCount - 1) * (float)fps);
                AudioSource.UnPause();
            }

            _frameTracker = startFrame;
        }
#endif

    #endregion

    #region LOAD_MESH_FUNCTIONS

    private void ShowFrame(VologramMesh[] meshes)
    {
        if (meshes == null || meshes.Length <= 0) return;

        Frame = meshes[0].Id;

#if UNITY_EDITOR
        Mesh mesh = _filter.sharedMesh;
#else
        Mesh mesh = _filter.mesh;
#endif
        
        meshes[0].UpdateMesh(ref mesh);

        if (IsVideoEnabled) return;

#if UNITY_EDITOR
        Material material = _renderer.sharedMaterial;
#else
        Material material = _renderer.material;
#endif
        meshes[0].UpdateTexture(ref material);

    }

    private void PlayNextFrame()
    {
        if (string.IsNullOrEmpty(_assetPath.Path))
            return;

        if (_assetBuffer != null && IsPlaying && _frameTracker < Header.TotalFrameCount - 1)
        {
            if (IsAudioEnabled && Mathf.Abs(AudioSource.time - (_frameTracker * fps)) > AudioSyncThreshold)
            {
                AudioSource.time = _frameTracker * fps;
            }
            ShowFrame(_assetBuffer.RetrieveFrameMesh(_frameTracker));

            if (boomerangLoop)
            {
                if (_boomerangForward)
                    _frameTracker++;
                else
                    _frameTracker--;
            }
            else
            {
                _frameTracker++;
            }
        }

        if (boomerangLoop)
        {
            if (_frameTracker >= Header.TotalFrameCount - 1 && _boomerangForward)
            {
                _frameTracker--;
                _boomerangForward = false;
            }
            else if (_frameTracker == startFrame && !_boomerangForward)
            {
                _boomerangForward = true;
            }
        }
        else
        {
            if (_frameTracker >= endFrame)
            {
                if (isLooping)
                    LoopCompleted();
                else 
                    Stop();
            }
        }
    }

    private void PlayFrame(int frameNumber)
    {
        if (string.IsNullOrEmpty(_assetPath.Path))
            return;

        _frameTracker = frameNumber;

        if (_frameTracker > endFrame)
            _frameTracker = endFrame;

        if (_frameTracker < startFrame)
            _frameTracker = startFrame;
        
        if (IsAudioEnabled && Mathf.Abs(AudioSource.time - (_frameTracker * fps)) > AudioSyncThreshold)
        {
            AudioSource.time = _frameTracker * fps;
        }
        
        ShowFrame(_assetBuffer.RetrieveFrameMesh(_frameTracker));
    }
    #endregion

    #region USER_FUNCTIONS
    /// <summary>
    /// Plays the vologram.
    /// </summary>
    public void Play()
    {
        if (!gameObject.activeInHierarchy || !gameObject.activeSelf || string.IsNullOrEmpty(_assetPath.Path))
            return;

        if (_updateRequired)
        {
            Awake();
            Setup();
            Init();
            if (!_assetBuffer.Buffering)
                _assetBuffer.StartBuffering();
            if (!playOnStart)
            {
                if (IsVideoEnabled)
                {
                    VideoTexturePlayer.sendFrameReadyEvents = true;
                    VideoTexturePlayer.Play();
                }

                if (IsAudioEnabled)
                {
                    AudioSource.time = audioStartTime;
                    AudioSource.Play();
                }
            }
        }
        else
        {
            if (!_assetBuffer.Buffering)
                _assetBuffer.StartBuffering();
            if (IsVideoEnabled)
            {
                VideoTexturePlayer.sendFrameReadyEvents = true;
                VideoTexturePlayer.Play();
            }

            if (IsAudioEnabled)
            {
                AudioSource.time = audioStartTime;
                AudioSource.Play();
            }
        }

        PlayerState = State.Playing;
        OnVologramPlay?.Invoke(this);
    }

    /// <summary>
    /// Plays the vologram after a specified amount of time (in seconds)
    /// </summary>
    /// <param name="time">Delay</param>
    public void PlayDelayed(float time)
    {
        StartCoroutine(PlayDelayed_(time));
    }

    /// <summary>
    /// Coroutine that waits for a specified amount of time 
    /// (in seconds) before playing the vologram.
    /// </summary>
    /// <param name="time">Delay</param>
    private IEnumerator PlayDelayed_(float time)
    {
        yield return new WaitForSeconds(time);

        Play();
    }

    /// <summary>
    /// Pauses the vologram.
    /// </summary>
    public void Pause()
    {
        if (IsAudioEnabled) 
            AudioSource.Pause();

        if (IsVideoEnabled)
        {
            VideoTexturePlayer.sendFrameReadyEvents = false;
            VideoTexturePlayer.Pause();
        }

        PlayerState = State.Paused;
        OnVologramPause?.Invoke(this);
    }

    /// <summary>
    /// Stops the vologram, resetting the frame to 0.
    /// </summary>
    public void Stop()
    {
        _assetBuffer?.StopBuffering();
        
        if (_filter.mesh != null)
#if UNITY_EDITOR
            _filter.sharedMesh.Clear();
#else
            _filter.mesh.Clear();
#endif

        if (IsAudioEnabled && gameObject.activeInHierarchy && gameObject.activeSelf)
            AudioSource.Stop();

        if (IsVideoEnabled)
        {
            VideoTexturePlayer.sendFrameReadyEvents = false;
            VideoTexturePlayer.Stop();
        }

        _frameTracker = 0;
        Frame = 0;
        PlayerState = State.Stopped;
        OnVologramStop?.Invoke(this);
    }

    /// <summary>
    /// Stops the vologram, resets the frame to 0 and starts 
    /// playing the vologram again.
    /// </summary>
    public void Restart()
    {
        Stop();
        Setup();
        Init();
        //Play();
    }

    public void Seek(int frameNumber)
    {
        if (IsVideoEnabled)
        {
            VideoTexturePlayer.frame = frameNumber;
        }
        else
        {
            if (IsAudioEnabled)
            {
                AudioSource.time = audioStartTime + ((float)startFrame / (float)(Header.TotalFrameCount - 1) * (float)fps);
                AudioSource.Pause();
            }
            PlayFrame(frameNumber);
            OnVologramSeekComplete?.Invoke(this, frameNumber);
        }
    }

    #endregion

#if UNITY_EDITOR
    #region PREVIEW_FUNCTIONS
    //UNITY EDITOR ONLY FUNCTIONS

    [HideInInspector]
    public bool editorIsLoadingFromFolder = true;

    [HideInInspector]
    public string editorSearchDirectoryCache = string.Empty;

    public void EditorAwake()
    {
        //Awake();
        //OnEnable();
    }

    public void EditorPrepForPreview()
    {
        if (_assetBuffer == null)
            Init();
    }

    public void EditorPlay(int frameNumber)
    {
        if (IsVideoEnabled && VideoTexturePlayer.isPrepared)
            VideoTexturePlayer.frame = frameNumber;
        else
            PlayFrame(frameNumber);
    }

    #endregion
#endif


    #region UTIL_FUNCTIONS

//     private bool CheckForVideoFile()
//     {
//         if (string.IsNullOrEmpty(_assetPath))
//             return false;
//
// #if UNITY_ANDROID && !UNITY_EDITOR
//         if (_assetPathType == PathType.StreamingAssets)
//             return BetterStreamingAssets.GetFiles(_assetPath, "*.mp4").Length > 0;
//         return Directory.GetFiles(FullAssetPath, "*.mp4").Length > 0; 
// #else
//         return Directory.GetFiles(FullAssetPath, "*.mp4").Length > 0;
// #endif
//     }
//
//     private string GetVideoUrl()
//     {
//         
// #if UNITY_ANDROID && !UNITY_EDITOR
//         if (_assetPathType != PathType.StreamingAssets) {
//             return Directory.GetFiles(FullAssetPath, "*.mp4")[0];
//         }
//
//         string androidUri = BetterStreamingAssets.GetFiles(_assetPath, "*.mp4")[0];
//         string outPath = Path.Combine(Application.persistentDataPath, androidUri);
//         FileInfo outInfo = new FileInfo(outPath);
//         if (outInfo.Exists)
//             return outPath;
//         if (!outInfo.Directory.Exists)
//             outInfo.Directory.Create();
//
//         using (Stream inStream = BetterStreamingAssets.OpenRead(androidUri)) {
//             using (FileStream outStream = outInfo.Exists ? outInfo.OpenWrite() : outInfo.Create())
//                 inStream.CopyTo(outStream, 10000000);
//         }
//         Debug.Log("Copied video to: " + outPath);
//         return outPath;
// #else
//         return "file://" + Directory.GetFiles(FullAssetPath, "*.mp4")[0];
// #endif
//     }
//
//     private (bool, bool) CheckForAudioFile()
//     {
//         if (useAbsolutePath)
//         {
//             if (Directory.GetFiles(AssetPath, "*.mp3").Length > 0)
//                 return (true, true);
//             if (Directory.GetFiles(AssetPath, "*.wav").Length > 0)
//                 return (true, false);
//         }
//         if (Directory.GetFiles(Path.Combine(Application.streamingAssetsPath, AssetPath), "*.mp3").Length > 0)
//             return (true, true);
//         if (Directory.GetFiles(Path.Combine(Application.streamingAssetsPath, AssetPath), "*.wav").Length > 0)
//             return (true, false);
//
//         return (false, false);
//     }
//
//     private AudioClip LoadMp3()
//     {
//         String file = useAbsolutePath ? 
//             Directory.GetFiles(AssetPath, "*.mp3")[0] :
//             Directory.GetFiles(Path.Combine(Application.streamingAssetsPath, AssetPath), "*.mp3")[0];
//         
//         if (VologramAudio != null && VologramAudio.name == file)
//             return VologramAudio;
//
//         AudioClip clip = null;
//         using (FileStream audioFileStream = File.OpenRead(file))
//         {
//             MpegFile mpegFile = new MpegFile(audioFileStream);
//             float[] samples = new float[mpegFile.Length];
//             mpegFile.ReadSamples(samples, 0, (int)mpegFile.Length);
//             clip = AudioClip.Create(file, samples.Length, mpegFile.Channels, mpegFile.SampleRate, false);
//             clip.SetData(samples, 0);
//         }
//         return clip;
//     }

    private void LoadHeader()
    {
        string path = Path.Combine(_assetPath.FullPath, "header.vols");
        if (!File.Exists(path))
        {
            Header = new VologramsHeader();
            return;
        }

        VologramHeaderReader headerReader = new VologramHeaderReader(path);
        Header = headerReader.GetHeader();
        endFrame = Header.TotalFrameCount - 1;
    }

    #endregion
}

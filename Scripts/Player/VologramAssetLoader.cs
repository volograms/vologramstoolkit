﻿// Copyright (C) 2018 Volograms Limited. All rights reserved.
//
// This file is part of Volograms Toolkit SDK.
//
// See LICENSE in the project root for license information.

using System;
using System.IO;
using UnityEngine;
using VologramsToolkit.Scripts.Serialization;

/// <summary>
///     Class for loading vologram frame data
/// </summary>
public class VologramAssetLoader
{
    private VologramFrame _vologramFrame;

    #region PROPERTIES

    /// <summary>
    ///     Gets the .vols files version.
    /// </summary>
    /// <value>The version.</value>
    public int Version { get; private set; }

    /// <summary>
    ///     Gets the index of the last frame.
    /// </summary>
    /// <value>The last frame.</value>
    public int LastFrame { get; private set; }

    public int LastPossibleFrame
    {
        get => _vologramFrame.LastFrame;
    }

    /// <summary>
    ///     Gets the loaded mesh data.
    /// </summary>
    /// <value>The loaded mesh data</value>
    public VologramMesh[] LoadedMeshes { get; private set; }

    public Vector3 TranslationAdjustment => _vologramFrame.Translation;

    public Quaternion RotationAdjustment => _vologramFrame.Rotation;

    public Vector3 ScaleAdjustment => _vologramFrame.Scale;

    public string ShaderName => _vologramFrame.Header.Shader;

    /// <summary>
    ///     Get the number of frames
    /// </summary>
    /// <returns>The frame count.</returns>
    public int FrameCount => _vologramFrame.LastFrame - _vologramFrame.StartFrame + 1;

    #endregion

    #region INITIALISATION

    /// <summary>
    ///     Initialise this <see cref="T:VOLAssetLoader" />.
    /// </summary>
    /// <param name="assetPath">Path to the .vols files</param>
    /// <param name="startFrame">Number of the first frame</param>
    public VologramAssetLoader(string assetPath, int startFrame)
    {
        SetUpVologramFrame(assetPath, startFrame);
    }

    private void SetUpVologramFrame(string assetPath, int startFrame)
    {
        LoadedMeshes = new VologramMesh[0];

        _vologramFrame = new VologramFrame(assetPath, startFrame);

        Version = _vologramFrame.Version;
        LastFrame = _vologramFrame.LastFrame;

        if (startFrame > 0)
            CreateMesh(true);
        else
            CreateMesh();
    }

    /// <summary>
    ///     Reset this <see cref="T:VOLAssetLoader" />.
    /// </summary>
    public void Reset(string newPath)
    {
        _vologramFrame.Reset(newPath);
        CreateMesh();
    }

    #endregion

    #region LOADING

    /// <summary>
    ///     Loads the mesh.
    /// </summary>
    /// <returns>Mesh data</returns>
    public VologramMesh[] LoadNextMesh()
    {
        return LoadNextVolsMesh();
    }

    /// <summary>
    ///     Loads the mesh data of a specified frame (with keyframe data)
    /// </summary>
    /// <returns>Mesh data</returns>
    /// <param name="frameNumber">Frame number</param>
    public VologramMesh[] LoadFrameNumber(int frameNumber)
    {
        if (_vologramFrame == null) return null;
        _vologramFrame.ReadBodyNumber(frameNumber);

        return CreateMesh(true);

    }

    private VologramMesh[] LoadNextVolsMesh()
    {
        _vologramFrame.ReadNextBody();
        return CreateMesh();
    }

    private VologramMesh[] CreateMesh(bool forceKeyFrame = false)
    {
        if (_vologramFrame != null)
        {
            VologramMesh[] meshes = new VologramMesh[1];
            meshes[0] = new VologramMesh(_vologramFrame.CurrentFrame, Version, _vologramFrame.Vertices,
                _vologramFrame.CurrentFrame.ToString(), _vologramFrame.MeshTopology);

            if (_vologramFrame.IsKeyFrame || forceKeyFrame)
            {
                if (_vologramFrame.UsingShortIndices)
                    meshes[0].AddKeyFrameData(_vologramFrame.Uvs, _vologramFrame.IndicesS);
                else
                    meshes[0].AddKeyFrameData(_vologramFrame.Uvs, _vologramFrame.Indices);
            }
            if (_vologramFrame.HasNormals) meshes[0].AddNormals(_vologramFrame.Normals);
            if (_vologramFrame.Textured)
                meshes[0].AddTextureData(_vologramFrame.TextureWidth, _vologramFrame.TextureHeight,
                    _vologramFrame.TextureData, _vologramFrame.TextureFormat);

            LoadedMeshes = meshes;
            return meshes;
        }

        return null;
    }

    public VologramsHeader GetHeader()
    {
        return _vologramFrame.Header.GetHeader();
    }

    #endregion

#if UNITY_EDITOR
    #region PREVIEW

    /// <summary>
    ///     Loads the mesh data for preview in editor.
    /// </summary>
    /// <returns>The frame in editor.</returns>
    /// <param name="assetPath">Path to the .vols files</param>
    /// <param name="frameNumber">Number of frame to be previewed</param>
    public VologramMesh[] LoadFrameInEditor(string assetPath, int frameNumber)
    {
        VologramMesh[] meshes = new VologramMesh[1];
        try
        {
            if (_vologramFrame == null ||
                !_vologramFrame.FilePath.Equals(Path.Combine(Application.streamingAssetsPath, assetPath)))
                _vologramFrame = new VologramFrame(assetPath);

            _vologramFrame.ReadBodyNumber(frameNumber);

            GameObject o = new GameObject();
            o.transform.position = _vologramFrame.Translation;
            o.transform.rotation = _vologramFrame.Rotation;
            o.transform.localScale = _vologramFrame.Scale;

            meshes[0] = new VologramMesh(_vologramFrame.CurrentFrame,
                _vologramFrame.Version, _vologramFrame.Vertices,
                _vologramFrame.CurrentFrame.ToString(), _vologramFrame.MeshTopology);
            if (_vologramFrame.UsingShortIndices)
                meshes[0].AddKeyFrameData(_vologramFrame.Uvs, _vologramFrame.IndicesS);
            else
                meshes[0].AddKeyFrameData(_vologramFrame.Uvs, _vologramFrame.Indices);

            if (_vologramFrame.HasNormals)
                meshes[0].AddNormals(_vologramFrame.Normals);

            if (_vologramFrame.Textured)
                meshes[0].AddTextureData(_vologramFrame.TextureWidth,
                    _vologramFrame.TextureHeight, _vologramFrame.TextureData,
                    _vologramFrame.TextureFormat);

            return meshes;
        }
        catch (Exception e)
        {
            Debug.LogWarning("Could not load frame in editor: \n" + e.Message + "\n" + e.StackTrace);
            return new VologramMesh[0];
        }
    }

    #endregion

#endif
}


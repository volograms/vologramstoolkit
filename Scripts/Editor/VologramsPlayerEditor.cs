﻿// Copyright (C) 2018 Volograms Limited. All rights reserved.
//
// This file is part of Volograms Toolkit SDK.
//
// See LICENSE in the project root for license information.
#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Video;
using NLayer;
using Obj = UnityEngine.Object;
using VClip = UnityEngine.Video.VideoClip;

namespace VologramsToolkit.Scripts.Editor
{
    [CustomEditor(typeof(VologramsPlayer))]
    public class VologramsPlayerEditor : UnityEditor.Editor
    {
        private VologramsPlayer _player;
        private int _previewframeNumber;
        private bool _previewIsPlaying;
        private float _previewTimeTracker;
        private float _previewPlaySpeed = 10f;
        private float PreviewFrameRate => 1f / _previewPlaySpeed;

        private VologramsPlayer.PathType _inspectorMeshPathType;
        private VologramsPlayer.PathType _inspectorVideoPathType;
        private string _inspectorMeshPath;
        private string _inspectorVideoPath;

        private GUIStyle _vologramsFoldoutStyle;
        private GUIStyle _vologramsInfoLabelStyle;

        // Resources
        private Texture _folderIcon;

        // General settings vars
        private bool _showPlaySettings = true;
        private bool _showTransformSettings = false;

        private string SearchDirectory
        {
            get => _player.editorSearchDirectoryCache;
            set => _player.editorSearchDirectoryCache = value;
        }

        // Load from Folder vars
        private bool _assetFolderExists;
        private bool _assetFolderHasHeader;
        private bool _assetFolderHasSequence;
        private string[] _assetFolderVideoFiles = new string[0];
        private string[] _assetFolderAudioFiles = new string[0];
        private string[] _assetFolderVideoFilesDisplayNames = new string[0];
        private string[] _assetFolderAudioFilesDisplayNames = new string[0];
        private int _assetFolderVideoSelection;
        private int _assetFolderAudioSelection;
        
        // Load from Files vars 
        private bool _meshFolderExists;
        private bool _meshFolderHasHeader;
        private bool _meshFolderHasSequence;

        private bool AssetIsValid => _meshFolderExists && _meshFolderHasHeader && _meshFolderHasSequence;

        // Component hide flags 
        private bool HideVideoComponent
        {
            get => _player.VideoTexturePlayer.hideFlags == HideFlags.HideInInspector;
            set => _player.VideoTexturePlayer.hideFlags = value ? HideFlags.HideInInspector : HideFlags.None;
        }

        private bool HideAudioComponent
        {
            get => _player.AudioSource.hideFlags == HideFlags.HideInInspector;
            set => _player.AudioSource.hideFlags = value ? HideFlags.HideInInspector : HideFlags.None;
        }

        //  This is called everytime the node is selected
        private void Awake()
        {
            _player = (VologramsPlayer)target;
            _player.EditorAwake();
            _folderIcon = Resources.Load<Texture>("folder-vol");

            _inspectorMeshPathType = _player.AssetPath.PathType;
            _inspectorVideoPathType = _player.VideoTextureUrl.PathType;
            _inspectorMeshPath = _player.AssetPath.Path;
            _inspectorVideoPath = _player.VideoTextureUrl.Path;

            InitCheckers();
        }

        private void OnEnable()
        {
            EditorApplication.update -= PlayInEditor;
            EditorApplication.update += PlayInEditor;
        }
        
        private void OnDisable()
        {
            EditorApplication.update -= PlayInEditor;
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitCheckers()
        {
            if (Application.isPlaying)
                return;
            
            ValidateMeshFolder();
        }
        
        public override void OnInspectorGUI()
        {
            _vologramsFoldoutStyle = new GUIStyle(EditorStyles.foldout)
            {
                fontStyle = FontStyle.Bold
            };

            _vologramsInfoLabelStyle = new GUIStyle(EditorStyles.label)
            {
                wordWrap = true
            };

            if (!(EditorApplication.isPlaying || EditorApplication.isPaused))
            {
                UpdatePlayerSettingsGUI();
            }
            else
            {
                DrawPlayInfoGUI();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdatePlayerSettingsGUI()
        {
            DrawMainPlayerFieldsGUI();

            EditorGUILayout.Space();

            DrawPlayerSettingsGUI();

            EditorGUILayout.Space();

            DrawTransformAdjustmentGUI();

            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Editor Preview", EditorStyles.boldLabel);
            if (string.IsNullOrEmpty(_player.AssetPath.Path))
            {
                EditorGUILayout.HelpBox("Add a vologram asset to enable editor preview", MessageType.Warning);
            }
            else
            {
                UpdateEditorPreviewGUI();
            }
            EditorGUILayout.Separator();
            DrawComponentHideButtons();
            EditorGUILayout.Separator();
        }

        /// <summary>
        /// Draws the main Inspector 
        /// </summary>
        private void DrawMainPlayerFieldsGUI()
        {
            // Main settings
            EditorGUILayout.LabelField("Main Settings", EditorStyles.boldLabel);
            EditorGUI.BeginDisabledGroup(_previewIsPlaying);
            DrawMeshFolderSettingsGUI();
            DrawVideoTextureSettingsGUI();
            DrawAudioSettingsGUI();
            EditorGUI.EndDisabledGroup();
            
            EditorGUILayout.Separator();
        }

        // private void DrawAssetsFolderSettingsGUI()
        // {
        //     EditorGUI.BeginChangeCheck();
        //     _filePathType = EditorGUILayout.Popup("Assets Folder", _filePathType, _filePathTypeText);
        //     if (EditorGUI.EndChangeCheck())
        //     {
        //         _player.UseAbsolutePath = _filePathType == 0;
        //         ValidateAssetFolder(_player.AssetPath, _player.UseAbsolutePath);
        //     }
        //
        //     EditorGUI.BeginChangeCheck();
        //     _player.AssetPath = EditorGUILayout.TextField("", _player.AssetPath);
        //     if (EditorGUI.EndChangeCheck())
        //     {
        //         ValidateAssetFolder(_player.AssetPath, _player.UseAbsolutePath);
        //     }
        //     
        //     EditorGUILayout.Separator();
        //     DrawOpenFolderExplorerButton("Open Vologram Asset Folder", ProcessOpenAssetFolder);
        //     EditorGUILayout.Separator();
        //     
        //     if (!_assetFolderExists) 
        //         EditorGUILayout.HelpBox("Folder does not exist", MessageType.Error, true);
        //     else
        //     {
        //         if (!_assetFolderHasHeader)
        //             EditorGUILayout.HelpBox("No header.vols file was found in the folder", MessageType.Error, true);
        //         else
        //             EditorGUILayout.LabelField("Header file", "Found");
        //
        //         if (!_assetFolderHasSequence)
        //             EditorGUILayout.HelpBox("No sequence_0.vols file was found in the folder", MessageType.Error, true);
        //         else
        //             EditorGUILayout.LabelField("Sequence file", "Found");
        //     }
        //
        //     EditorGUILayout.Separator();
        //     
        //     EditorGUI.BeginDisabledGroup(!_assetFolderExists || !_assetFolderHasHeader || !_assetFolderHasSequence);
        //     
        //     EditorGUI.BeginDisabledGroup(_assetFolderVideoFiles.Length == 0);
        //     EditorGUI.BeginChangeCheck();
        //     _assetFolderVideoSelection = EditorGUILayout.Popup("Select Video Texture", _assetFolderVideoSelection, _assetFolderVideoFilesDisplayNames);
        //     if (EditorGUI.EndChangeCheck())
        //     {
        //         _player.VideoTextureUrl = _assetFolderVideoFiles[_assetFolderVideoSelection];
        //     }
        //     EditorGUI.EndDisabledGroup();
        //     
        //     EditorGUI.BeginDisabledGroup(_assetFolderAudioFiles.Length == 0);
        //     EditorGUI.BeginChangeCheck();
        //     _assetFolderAudioSelection = EditorGUILayout.Popup("Select Audio", _assetFolderAudioSelection, _assetFolderAudioFilesDisplayNames);
        //     if (EditorGUI.EndChangeCheck())
        //     {
        //         _player.VologramAudio = LoadAudioFromFile(_assetFolderAudioFiles[_assetFolderAudioSelection]);
        //     }
        //     EditorGUI.EndDisabledGroup();
        //
        //     EditorGUI.EndDisabledGroup();
        // }

        /// <summary>
        /// Draws the Mesh Folder section of the Inspector
        /// </summary>
        private void DrawMeshFolderSettingsGUI()
        {
            EditorGUI.BeginChangeCheck();
            _inspectorMeshPathType = (VologramsPlayer.PathType)EditorGUILayout.EnumPopup("Path type", _player.AssetPath.PathType);
            _inspectorMeshPath = EditorGUILayout.DelayedTextField("Video Texture", _player.AssetPath.Path);
            if (EditorGUI.EndChangeCheck())
            {
                _player.AssetPath = new VologramsPlayer.VologramPath(_inspectorMeshPathType, _inspectorMeshPath);
                ValidateMeshFolder();
            }
            
            // EditorGUI.BeginChangeCheck();
            // _filePathType = EditorGUILayout.Popup("Mesh Folder", _filePathType, _filePathTypeText);
            // if (EditorGUI.EndChangeCheck())
            // {
            //     _player.UseAbsolutePath = _filePathType == 0;
            //     ValidateMeshFolder(_player.AssetPath, _player.UseAbsolutePath);
            // }
            //
            // EditorGUI.BeginChangeCheck();
            // _player.AssetPath = EditorGUILayout.TextField("", _player.AssetPath);
            // if (EditorGUI.EndChangeCheck())
            // {
            //     ValidateMeshFolder(_player.AssetPath, _player.UseAbsolutePath);
            // }

            DrawOpenFolderExplorerButton("Open Vologram Asset Folder", ProcessOpenMeshFolder);
            
            EditorGUILayout.Separator();
            if (!_meshFolderExists) 
                EditorGUILayout.HelpBox("Folder does not exist", MessageType.Error, true);
            else
            {
                if (!_meshFolderHasHeader)
                    EditorGUILayout.HelpBox("No header.vols file was found in the folder", MessageType.Error, true);
                else
                    EditorGUILayout.LabelField("Header file", "Found");

                if (!_meshFolderHasSequence)
                    EditorGUILayout.HelpBox("No sequence_0.vols file was found in the folder", MessageType.Error, true);
                else
                    EditorGUILayout.LabelField("Sequence file", "Found");
            }
            EditorGUILayout.Separator();

            EditorGUILayout.Space();
        }
        
        /// <summary>
        /// Draws the Video Texture section of the Inspector
        /// </summary>
        private void DrawVideoTextureSettingsGUI()
        {
            EditorGUI.BeginChangeCheck();
            _inspectorVideoPathType = (VologramsPlayer.PathType)EditorGUILayout.EnumPopup("Path type", _player.VideoTextureUrl.PathType);
            _inspectorVideoPath = EditorGUILayout.DelayedTextField("Video Texture", _player.VideoTextureUrl.Path);
            if (EditorGUI.EndChangeCheck())
            {
                _player.VideoTextureUrl =
                    new VologramsPlayer.VologramPath(_inspectorVideoPathType, _inspectorVideoPath);
            }
            
            DrawOpenFileExplorerButton("Open Vologram Video Texture", "mp4", ProcessOpenVideoTextureFile);

            EditorGUILayout.Separator();
        }
        
        /// <summary>
        /// Draws the Audio section of the inspector
        /// </summary>
        private void DrawAudioSettingsGUI()
        {
            _player.VologramAudio = EditorGUILayout.ObjectField("Audio", _player.VologramAudio,
                typeof(AudioClip), false) as AudioClip;
            
            DrawOpenFileExplorerButton("Open Vologram Audio", "mp3", ProcessOpenAudioFile);
            
            EditorGUILayout.Separator();
        }

        private void DrawPlayerSettingsGUI()
        {
            EditorGUI.BeginChangeCheck();

            _showPlaySettings = EditorGUILayout.Foldout(_showPlaySettings, "Play Settings", _vologramsFoldoutStyle);
            if (_showPlaySettings)
            {
                _player.PlayOnStart = EditorGUILayout.Toggle("Play on start", _player.PlayOnStart);
                _player.IsLooping = EditorGUILayout.Toggle("Loop", _player.IsLooping);
                _player.boomerangLoop = EditorGUILayout.Toggle("Boomerang Loop", _player.boomerangLoop);
                _player.MeshMaterial = EditorGUILayout.ObjectField("Material", _player.MeshMaterial, typeof(Material), false) as Material;

                if (!AssetIsValid)
                {
                    EditorGUILayout.HelpBox("Add a vologram asset to activate these settings", MessageType.Warning);
                }

                EditorGUI.BeginDisabledGroup(!AssetIsValid);
                _player.BufferSize = EditorGUILayout.IntSlider("Buffer size", _player.BufferSize, 2, 10);
                _player.AudioStartTime = EditorGUILayout.FloatField("Audio delay", _player.AudioStartTime);
                
                EditorGUI.BeginChangeCheck();
                _player.StartFrame = EditorGUILayout.IntSlider("Start Frame", _player.StartFrame, 0, _player.Header.TotalFrameCount - 1);
                _player.EndFrame = EditorGUILayout.IntSlider("End Frame (MAX: " + (_player.Header.TotalFrameCount - 1) + ")", _player.EndFrame, 0, _player.Header.TotalFrameCount - 1);
                if (EditorGUI.EndChangeCheck())
                {
                    if (_previewframeNumber < _player.StartFrame || _previewframeNumber > _player.EndFrame)
                    {
                        _previewframeNumber = _player.StartFrame;
                        _player.Pause();
                        _player.Seek(_previewframeNumber);
                    }
                }
                
                _player.FPS = EditorGUILayout.IntField("Frame Rate", _player.FPS);
                _player.PlaybackSpeed = EditorGUILayout.Slider("Playback Speed", _player.PlaybackSpeed, 0.5f, 2f);
                EditorGUI.EndDisabledGroup();
            }

            if (EditorGUI.EndChangeCheck())
                EditorUtility.SetDirty(target);
        }

        private void DrawTransformAdjustmentGUI()
        {
            EditorGUI.BeginChangeCheck();

            // Transform Menu
            _showTransformSettings = EditorGUILayout.Foldout(_showTransformSettings, "Vologram Transform Adjustment Settings", _vologramsFoldoutStyle);
            if (_showTransformSettings)
            {
                if (!AssetIsValid)
                {
                    EditorGUILayout.HelpBox("Add a vologram asset to activate these settings", MessageType.Warning);
                }
                EditorGUI.BeginDisabledGroup(!AssetIsValid);
                _player.adjustmentRequired = EditorGUILayout.Toggle("Activate", _player.adjustmentRequired);
                if (_player.adjustmentRequired)
                {
                    _player.positionAdj = EditorGUILayout.Vector3Field("Position", _player.positionAdj);
                    _player.rotationAdj = EditorGUILayout.Vector3Field("Rotation", _player.rotationAdj);
                    _player.scaleAdj = EditorGUILayout.Vector3Field("Scale", _player.scaleAdj);
                }
                EditorGUI.EndDisabledGroup();
            }

            if (EditorGUI.EndChangeCheck())
                EditorUtility.SetDirty(target);
        }

        private void UpdateEditorPreviewGUI()
        {
            if (!_player.isActiveAndEnabled)
            {
                EditorGUILayout.HelpBox("Vologram Player is inactive in scene. Activate to view preview.", MessageType.Warning);
                return;
            }
            
            EditorGUI.BeginChangeCheck();
            _previewPlaySpeed = EditorGUILayout.FloatField("Preview Play Speed", _previewPlaySpeed);
            if (EditorGUI.EndChangeCheck())
            {
                _previewPlaySpeed = Mathf.Clamp(_previewPlaySpeed, 1f, 20f);
            }

            EditorGUILayout.LabelField("Preview Frame", _previewframeNumber.ToString());
            EditorGUI.BeginChangeCheck();
            EditorGUI.BeginDisabledGroup(_player.IsPlaying);
            _previewframeNumber = EditorGUILayout.IntSlider(_previewframeNumber, _player.StartFrame, _player.EndFrame);
            EditorGUI.EndDisabledGroup();
            if (EditorGUI.EndChangeCheck())
            {
                _player.Seek(_previewframeNumber);
            }

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Prev frame"))
            {
                _previewframeNumber -= 1;
                if (_previewframeNumber < _player.StartFrame)
                    _previewframeNumber = _player.EndFrame;
                
                _player.EditorPrepForPreview();
                _player.Seek(_previewframeNumber);
            }
            
            if (_previewIsPlaying)
            {
                if (GUILayout.Button("Stop"))
                {
                    _previewIsPlaying = false;
                }
            }
            else
            {
                if (GUILayout.Button("Play"))
                {
                    _previewIsPlaying = true;
                }
            }

            if (GUILayout.Button("Next frame"))
            {
                _previewIsPlaying = false;
                _previewframeNumber = (_previewframeNumber + 1) % (_player.EndFrame + 1);
                _player.EditorPrepForPreview();
                _player.Seek(_previewframeNumber);
            }
            EditorGUILayout.EndHorizontal();
        }
        
        private void DrawComponentHideButtons()
        {
            EditorGUILayout.BeginHorizontal();
            
            HideVideoComponent = EditorGUILayout.Toggle("Hide Video Player", HideVideoComponent);
            HideAudioComponent = EditorGUILayout.Toggle("Hide Audio Player", HideAudioComponent);

            EditorGUILayout.EndHorizontal();
        }

        private void DrawPlayInfoGUI()
        {
            EditorGUILayout.LabelField("Info", EditorStyles.boldLabel);
            EditorGUILayout.HelpBox("To change settings, exit playmode", MessageType.Info);
            EditorGUILayout.LabelField($"Asset ({_player.AssetPath.PathType.ToString()}): ", _player.AssetPath.FullPath, _vologramsInfoLabelStyle);
            EditorGUILayout.LabelField($"Texture ({_player.VideoTextureUrl.PathType.ToString()}): ", _player.VideoTextureUrl.FullPath, _vologramsInfoLabelStyle);
            EditorGUILayout.LabelField("Audio: ", _player.VologramAudio == null ?
                    string.Empty : _player.VologramAudio.ToString(), _vologramsInfoLabelStyle);
            EditorGUILayout.LabelField("Frames: ", _player.StartFrame + " - " + _player.EndFrame, _vologramsInfoLabelStyle);
            EditorGUILayout.LabelField("Buffer Size: ", _player.BufferSize.ToString(), _vologramsInfoLabelStyle);
            EditorGUILayout.LabelField("Audio Delay: ", _player.AudioStartTime.ToString(), _vologramsInfoLabelStyle);
            EditorGUILayout.LabelField("Frame Rate: ", _player.FPS.ToString(), _vologramsInfoLabelStyle);
            DrawComponentHideButtons();
        }

        private void DrawOpenFolderExplorerButton(string buttonText, Action<string> callback)
        {
            if (!GUILayout.Button(new GUIContent(buttonText, _folderIcon), GUILayout.Height(30f))) return;
            string path = EditorUtility.OpenFolderPanel("Search Vologram Folder", SearchDirectory, "");
            if (string.IsNullOrEmpty(path)) return;
            SearchDirectory = path;
            callback.Invoke(path);
        }

        private void DrawOpenFileExplorerButton(string buttonText, string extension,
            Action<string> callback)
        {
            if (!GUILayout.Button(new GUIContent(buttonText, _folderIcon), GUILayout.Height(30f))) return;
            string path = EditorUtility.OpenFilePanel("Open Vologram Video Texture", SearchDirectory, extension);
            if (string.IsNullOrEmpty(path)) return;
            string[] pathComponents = path.Split('/');
            SearchDirectory = string.Join("/", pathComponents.Take(pathComponents.Length - 1).ToArray());
            callback.Invoke(path);
        }

        private void PlayInEditor()
        {
            if (Application.isPlaying || 
                _player == null ||
                !_player.isActiveAndEnabled ||
                !_previewIsPlaying)
                return;

            _previewTimeTracker += Time.deltaTime;
            if (_previewTimeTracker > PreviewFrameRate)
            {
                _previewframeNumber = (_previewframeNumber + 1) % (_player.EndFrame + 1);
                _player.EditorPrepForPreview();
                _player.EditorPlay(_previewframeNumber);
                _previewTimeTracker -= PreviewFrameRate;
            }
        }

        // private void ValidateAssetFolder(string path)
        // {
        //     string pathToCheck = path;
        //     _assetFolderExists = Directory.Exists(pathToCheck);
        //     if (!_assetFolderExists)
        //     {
        //         _assetFolderHasHeader = false;
        //         _assetFolderHasSequence = false;
        //         _assetFolderVideoFiles = new string[0];
        //         _assetFolderAudioFiles = new string[0];
        //         _assetFolderVideoFilesDisplayNames = new string[0];
        //         _assetFolderAudioFilesDisplayNames = new string[0];
        //         return;
        //     }
        //
        //     _assetFolderHasHeader = File.Exists(Path.Combine(pathToCheck, "header.vols"));
        //     _assetFolderHasSequence = File.Exists(Path.Combine(pathToCheck, "sequence_0.vols"));
        //     
        //     if (!_assetFolderHasHeader || !_assetFolderHasSequence)
        //     {
        //         _assetFolderVideoFiles = new string[0];
        //         _assetFolderAudioFiles = new string[0];
        //         _assetFolderVideoFilesDisplayNames = new string[0];
        //         _assetFolderAudioFilesDisplayNames = new string[0];
        //         return;
        //     }
        //     
        //     _assetFolderVideoFiles = Directory.GetFiles(pathToCheck, "*.mp4").ToArray();
        //     _assetFolderAudioFiles = Directory.GetFiles(pathToCheck, "*.mp3").ToArray();
        //
        //     _assetFolderVideoFilesDisplayNames = _assetFolderVideoFiles.Select(file => file.Split('/').Last()).ToArray();
        //     _assetFolderAudioFilesDisplayNames = _assetFolderAudioFiles.Select(file => file.Split('/').Last()).ToArray();
        //
        //     if (_assetFolderVideoFiles.Length > 0)
        //     {
        //         string videoCheck = _player.FullVideoTextureUrl.StartsWith("file://")
        //             ? _player.FullVideoTextureUrl.Remove(0, 7)
        //             : _player.FullVideoTextureUrl;
        //         _assetFolderVideoSelection = Array.IndexOf(_assetFolderVideoFiles, videoCheck);
        //     }
        //
        //     if (_assetFolderAudioFiles.Length > 0)
        //     {
        //         _assetFolderAudioSelection = _player.VologramAudio == null
        //             ? _assetFolderAudioFiles.Length - 1
        //             : Array.IndexOf(_assetFolderAudioFiles, _player.AudioSource.clip.name);
        //     }
        // }

        // private void ProcessOpenAssetFolder(string path)
        // {
        //     if (string.IsNullOrEmpty(path))
        //         return;
        //     
        //     if (path.StartsWith(Application.streamingAssetsPath))
        //     {
        //         _filePathType = 1;
        //         _player.AssetPath = (VologramsPlayer.PathType.StreamingAssets, path.Remove(0, Application.streamingAssetsPath.Length + 1));
        //     }
        //     else if (path.StartsWith(Application.persistentDataPath))
        //     {
        //         
        //     }
        //     else
        //     {
        //         _filePathType = 0;
        //         _player.UseAbsolutePath = true;
        //         _player.AssetPath = path;
        //     }
        //
        //     _assetFolderExists = Directory.Exists(path);
        //
        //     _assetFolderHasHeader = File.Exists(Path.Combine(path, "header.vols"));
        //     _assetFolderHasSequence = File.Exists(Path.Combine(path, "sequence_0.vols"));
        //
        //     if (!_assetFolderHasHeader || !_assetFolderHasSequence)
        //     {
        //         _assetFolderVideoFiles = new string[0];
        //         _assetFolderAudioFiles = new string[0];
        //         _assetFolderVideoFilesDisplayNames = new string[0];
        //         _assetFolderAudioFilesDisplayNames = new string[0];
        //         return;
        //     }
        //
        //     _assetFolderVideoFiles = Directory.GetFiles(path, "*.mp4").ToArray();
        //     _assetFolderAudioFiles = Directory.GetFiles(path, "*.mp3").ToArray();
        //
        //     _assetFolderVideoFilesDisplayNames = _assetFolderVideoFiles.Select(file => file.Split('/').Last()).ToArray();
        //     _assetFolderAudioFilesDisplayNames = _assetFolderAudioFiles.Select(file => file.Split('/').Last()).ToArray();
        //
        //     if (_assetFolderVideoFiles.Length > 0)
        //     {
        //         _player.VideoTextureUrl = _assetFolderVideoFiles[0];
        //         _assetFolderVideoSelection = 0;
        //     }
        //     else
        //     {
        //         _player.VideoTextureUrl = string.Empty;
        //     }
        //
        //     if (_assetFolderAudioFiles.Length > 0)
        //     {
        //         _player.VologramAudio = LoadAudioFromFile(_assetFolderAudioFiles[_assetFolderAudioSelection]);
        //         _assetFolderAudioSelection = 0;
        //     }
        //     else
        //     {
        //         _player.VologramAudio = null;
        //     }
        // }

        private void ValidateMeshFolder()
        {
            string pathToCheck = _player.AssetPath.FullPath;
            _meshFolderExists = Directory.Exists(pathToCheck);
            if (!_meshFolderExists)
            {
                _meshFolderHasHeader = false;
                _meshFolderHasSequence = false;
                return;
            }

            _meshFolderHasHeader = File.Exists(Path.Combine(pathToCheck, "header.vols"));
            _meshFolderHasSequence = File.Exists(Path.Combine(pathToCheck, "sequence_0.vols"));
        }

        private void ProcessOpenMeshFolder(string path)
        {
            if (string.IsNullOrEmpty(path))
                return;

            _player.AssetPath = VologramsPlayer.VologramPath.InterpretFromPath(path);

            _meshFolderExists = Directory.Exists(path);
            _meshFolderHasHeader = File.Exists(Path.Combine(path, "header.vols"));
            _meshFolderHasSequence = File.Exists(Path.Combine(path, "sequence_0.vols"));

            if (CheckForVideoFile())
            {
                if (EditorUtility.DisplayDialog("Video file found",
                    $"Found video file {GetVideoUrl().Split('/').Last()}, would you like to use it?", "Yes", "No"))
                {
                    ProcessOpenVideoTextureFile(GetVideoUrl());
                }
            }
        }

        private void ProcessOpenVideoTextureFile(string file)
        {
            _player.VideoTextureUrl = VologramsPlayer.VologramPath.InterpretFromPath(file);
        }

        private bool CheckForVideoFile()
        {
            return Directory.GetFiles(_player.AssetPath.FullPath, "*.mp4").Length > 0;
        }

        private string GetVideoUrl()
        {
            return Directory.GetFiles(_player.AssetPath.FullPath, "*.mp4")[0];
        }

        private void ProcessOpenAudioFile(string file)
        {
            _player.VologramAudio = LoadAudioFromFile(file);
        }

        private AudioClip LoadAudioFromFile(string file)
        {
            if (string.IsNullOrEmpty(file))
                return null;
            
            if (_player.VologramAudio != null && _player.VologramAudio.name == file)
                return _player.VologramAudio;
            
            AudioClip clip = null;
            using (FileStream audioFileStream = File.OpenRead(file))
            {
                MpegFile mpegFile = new MpegFile(audioFileStream);
                float[] samples = new float[mpegFile.Length];
                mpegFile.ReadSamples(samples, 0, (int)mpegFile.Length);
                clip = AudioClip.Create(file, samples.Length, mpegFile.Channels, mpegFile.SampleRate, false);
                clip.SetData(samples, 0);
            }

            return clip;
        }
    }
}
#endif

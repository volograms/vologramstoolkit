﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace VologramsToolkit.Scripts.Serialization
{
    public static class VologramMetaData
    {
        [Serializable]
        public struct VolsHeaderMetaData
        {
            public int version;
            public int frameCount;
            public int meshTopology;
            public int textureWidth;
            public int textureHeight;
            public int textureFormat;
        }

        [Serializable]
        public struct VolsBodyMetaData
        {
            public int frameNumber;
            public int lastKeyFrameNumber;
            public byte keyFrame;
            public int vertices;
            public int indices;
            public int uvs;
            public bool usingShortIndices;
            public int normals;
            public int texture;
            public int fileNumber;
            public long byteLength;
            public long bytePosition;
        }

        [Serializable]
        public struct VolsMetaDataFile
        {
            public VolsHeaderMetaData headerMetaData;
            public List<VolsBodyMetaData> bodyMetaData;
        }

        public static void SaveMetaData(string vologramsPath)
        {
            VologramFrame frame = new VologramFrame(vologramsPath);

            VolsHeaderMetaData headerMetaData = frame.Header.GetMetaData();
            List<VolsBodyMetaData> bodyMetaData = new List<VolsBodyMetaData>();

            for (int i = 0; i < frame.FrameCount; i++)
            {
                bodyMetaData.Add(frame.Body.GetMetaData());
                frame.ReadNextBody();
            }

            VolsMetaDataFile volsMeta = new VolsMetaDataFile
            {
                headerMetaData = headerMetaData,
                bodyMetaData = bodyMetaData
            };

            string outputPath = Path.Combine(Application.streamingAssetsPath, vologramsPath, "meta.json");
            Debug.Log("Writing to: " + outputPath);
            File.WriteAllText(outputPath, JsonUtility.ToJson(volsMeta));
        }

        public static IEnumerator SaveMetaDataAsync(string vologramsPath)
        {
            VologramFrame frame = new VologramFrame(vologramsPath);

            VolsHeaderMetaData headerMetaData = frame.Header.GetMetaData();
            List<VolsBodyMetaData> bodyMetaData = new List<VolsBodyMetaData>();

            for (int i = 0; i < frame.FrameCount; i++)
            {
                bodyMetaData.Add(frame.Body.GetMetaData());
                frame.ReadNextBody();
                Debug.Log("Read frame: " + i);
                yield return 0;
            }

            VolsMetaDataFile volsMeta = new VolsMetaDataFile
            {
                headerMetaData = headerMetaData,
                bodyMetaData = bodyMetaData
            };

            string outputPath = Path.Combine(Application.streamingAssetsPath, vologramsPath, "meta.json");
            using (FileStream stream = File.OpenWrite(outputPath))
            using (StreamWriter writer = new StreamWriter(stream))
            {
                yield return writer.WriteAsync(JsonUtility.ToJson(volsMeta));
            }
        }

        public static VolsMetaDataFile LoadMetaData(string metaDataFile)
        {
            string metaDataText = File.ReadAllText(metaDataFile);
            VolsMetaDataFile meta = JsonUtility.FromJson<VolsMetaDataFile>(metaDataText);
            return meta;
        }

        /*
     * VolsHeaderReader extension methods
     */
        private static VolsHeaderMetaData GetMetaData(this VologramHeaderReader headerReader)
        {
            VolsHeaderMetaData data = new VolsHeaderMetaData
            {
                version = headerReader.Version,
                frameCount = headerReader.FrameCount,
                meshTopology = headerReader.Topology,
                textureWidth = headerReader.TextureWidth,
                textureHeight = headerReader.TextureHeight,
                textureFormat = headerReader.TextureFormat
            };
            return data;
        }

        /*
     * VolsBodyReader extension methods
     */
        private static VolsBodyMetaData GetMetaData(this VologramBodyReader bodyReader)
        {
            VolsBodyMetaData data = new VolsBodyMetaData
            {
                frameNumber = bodyReader.FrameNumber,
                lastKeyFrameNumber = bodyReader.LastKeyFrameNumber,
                keyFrame = bodyReader.Keyframe,
                vertices = bodyReader.VerticesData.Length,
                indices = bodyReader.IndicesData.Length,
                uvs = bodyReader.UvsData.Length,
                usingShortIndices = bodyReader.UsingShortIndices,
                normals = bodyReader.NormalsData.Length,
                texture = bodyReader.TextureData.Length,
                fileNumber = bodyReader.FrameStartFile,
                byteLength = bodyReader.MeshDataSize,
                bytePosition = bodyReader.FrameStartPosition
            };
            return data;
        }
    }
}

﻿// Copyright (C) 2018 Volograms Limited. All rights reserved.
//
// This file is part of Volograms Toolkit SDK.
//
// See LICENSE in the project root for license information.

using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;

namespace VologramsToolkit.Scripts.Serialization
{
    /// <summary>
    ///     Class that handles the reading and storing of mesh data.
    /// </summary>
    public class VologramFrame
    {
        #region VARIABLES

        public VologramHeaderReader Header { get; private set; }
        public VologramBodyReader Body { get; private set; }

        #endregion

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:VOLSFrame" /> class.
        /// </summary>
        /// <param name="filePath">Path to the .vols files</param>
        /// <param name="startFrame">Number of the first frame</param>
        public VologramFrame(string filePath, int startFrame = 0)
        {
            Initialize(filePath, startFrame);
        }

        private void Initialize(string filePath, int startFrame = 0)
        {
            FilePath = filePath;

            string headerPath = Path.Combine(FilePath, "header.vols");

            Header = new VologramHeaderReader(headerPath);
            ReadHeaderData();

            StartFrame = startFrame;

            string[] files = Directory.GetFiles(FilePath, "sequence_*.vols");

            string[] sortedFiles = new string[files.Length];
            foreach (string f in files)
            {
                const string pattern = @"sequence_([0-9]+).vols";
                Match m = Regex.Match(f, pattern);
                int sortedIndex = int.Parse(m.Groups[1].Value);
                sortedFiles[sortedIndex] = f;
            }

            bool useStreamingAssets = filePath.StartsWith(Application.streamingAssetsPath);
            Body = new VologramBodyReader(useStreamingAssets, Version, sortedFiles);
            ConfigBodyReader();

            if (startFrame > 0)
                ReadBodyNumber(startFrame);
            else
                ReadNextBody();
        }

#region PROPERTIES

        /// <summary>
        ///     Gets or sets the path to the .vols files.
        /// </summary>
        /// <value>The .vols path</value>
        public string FilePath { get; private set; }

        /// <summary>
        ///     Index of the first frame to be read.
        /// </summary>
        /// <value>The first frame index</value>
        public int StartFrame { get; private set; }

        /// <summary>
        ///     Gets or sets the index of the last frame.
        /// </summary>
        /// <value>The last frame.</value>
        public int LastFrame { get; private set; }

        public int FrameCount => Header.FrameCount;

        /// <summary>
        ///     Gets a value indicating whether this <see cref="T:VOLSFrame" /> is key frame.
        /// </summary>
        /// <value><c>true</c> if is key frame; otherwise, <c>false</c></value>
        public bool IsKeyFrame => Body.Keyframe == 0x1;

        /// <summary>
        ///     Gets the current frame.
        /// </summary>
        /// <value>The current frame</value>
        public int CurrentFrame => Body.FrameNumber;

        /// <summary>
        ///     Gets a value indicating whether this <see cref="T:VOLSFrame" /> uses shorts for indices.
        /// </summary>
        /// <value><c>true</c> if using short indices; otherwise, <c>false</c></value>
        public bool UsingShortIndices => Body.UsingShortIndices;

        // Added in Version 1.0

        /// <summary>
        ///     Gets or sets the vols version.
        /// </summary>
        /// <value>The version.</value>
        public int Version => Header.Version;

        /// <summary>
        ///     Gets or sets the mesh vertices.
        /// </summary>
        /// <value>The vertices</value>
        public Vector3[] Vertices { get; private set; }

        /// <summary>
        ///     Gets or sets the mesh uvs.
        /// </summary>
        /// <value>The uvs</value>
        public Vector2[] Uvs { get; private set; }

        /// <summary>
        ///     Gets or sets the mesh indices.
        /// </summary>
        /// <value>The indices</value>
        public int[] Indices { get; private set; }
        public ushort[] IndicesS { get; private set; }

        /// <summary>
        ///     Gets or sets the mesh topology.
        /// </summary>
        /// <value>The mesh topology</value>
        public MeshTopology MeshTopology { get; private set; }

        /// <summary>
        ///     Gets or sets the name of the frame mesh.
        /// </summary>
        /// <value>The name of the frame</value>
        public string MeshName => Header.MeshName;

        // Added in Version 1.1

        /// <summary>
        ///     Gets or sets a value indicating whether this <see cref="T:VOLSFrame" /> has normals.
        /// </summary>
        /// <value><c>true</c> if has normals; otherwise, <c>false</c></value>
        public bool HasNormals => Header.Normals == 0x1;

        /// <summary>
        ///     Gets or sets a value indicating whether this <see cref="T:VOLSFrame" /> is textured.
        /// </summary>
        /// <value><c>true</c> if textured; otherwise, <c>false</c></value>
        public bool Textured => Header.Textured == 0x1;

        /// <summary>
        ///     Gets or sets the width of the texture.
        /// </summary>
        /// <value>The width of the texture</value>
        public ushort TextureWidth => Header.TextureWidth;

        /// <summary>
        ///     Gets or sets the height of the texture.
        /// </summary>
        /// <value>The height of the texture</value>
        public ushort TextureHeight => Header.TextureHeight;

        /// <summary>
        ///     Gets or sets the texture format.
        /// </summary>
        /// <value>The texture format</value>
        public TextureFormat TextureFormat { get; private set; }

        /// <summary>
        ///     Gets or sets the mesh normals.
        /// </summary>
        /// <value>The normals</value>
        public Vector3[] Normals { get; private set; }

        /// <summary>
        ///     Gets or sets the texture data.
        /// </summary>
        /// <value>The texture data</value>
        public byte[] TextureData { get; private set; }

        // Added in Version 1.2

        /// <summary>
        ///     Gets or sets the adjustment translation.
        /// </summary>
        /// <value>The adjustment translation</value>
        public Vector3 Translation { get; private set; }

        /// <summary>
        ///     Gets or sets the adjustment rotation.
        /// </summary>
        /// <value>The adjustment rotation</value>
        public Quaternion Rotation { get; private set; }

        /// <summary>
        ///     Gets or sets the adjustment scale.
        /// </summary>
        /// <value>The adjustment scale</value>
        public Vector3 Scale { get; private set; }

#endregion

#region READING

        /// <summary>
        ///     Reads the data for the next frame.
        /// </summary>
        public void ReadNextBody()
        {
            Body.ReadV11(HasNormals, Textured);
            ParseBody();
        }

        /// <summary>
        ///     Reads the data for a specified frame.
        /// </summary>
        /// <param name="frameNumber">Frame number.</param>
        public void ReadBodyNumber(int frameNumber)
        {
            bool isMovingBackwards = IsMovingBackwards(Body.FrameNumber, frameNumber);
            Body.SeekV11(HasNormals, Textured, frameNumber, isMovingBackwards);
            ForceParseFullBody();
        }

        private void ReadHeaderData()
        {
            LastFrame = Header.FrameCount - 1;
            MeshTopology = (MeshTopology) Header.Topology;

            if (Version >= 11)
                TextureFormat = (TextureFormat) Header.TextureFormat;
            else
                TextureFormat = TextureFormat.Alpha8;

            if (Version >= 12)
            {
                // Default Transformation
                Translation = new Vector3(Header.Translation[0], Header.Translation[1], Header.Translation[2]);
                Rotation = new Quaternion(Header.Rotation[0], Header.Rotation[1], Header.Rotation[2],
                    Header.Rotation[3]);
                Scale = new Vector3(Header.Scale, Header.Scale, Header.Scale);
            }
            else
            {
                Translation = new Vector3(0, 0, 0);
                Rotation = new Quaternion(0, 0, 0, 1);
                Scale = new Vector3(1, 1, 1);
            }
        }

#endregion

#region PARSING

        private void ParseBody()
        {
            ParseVerticesData();

            if (HasNormals)
                ParseNormalsData();
            else
                Normals = new Vector3[0];

            if (IsKeyFrame)
            {
                ParseIndicesData();
                ParseUvsData();
            }
            // else use the indices and uvs from the previous frame

            if (Textured)
                ParseTextureData();
            else
                TextureData = new byte[0];
        }

        private void ForceParseFullBody()
        {
            ParseVerticesData();

            if (HasNormals)
                ParseNormalsData();
            else
                Normals = new Vector3[0];

            ParseIndicesData();
            ParseUvsData();

            if (Textured)
                ParseTextureData();
            else
                TextureData = new byte[0];
        }

        private void ParseVerticesData()
        {
            Vertices = Body.VerticesData;
            //Vertices = new Vector3[Body.VerticesData.Length / 3];

            //for (int i = 0, dataIdx = 0; i < Vertices.Length; ++i, dataIdx += 3)
            //    Vertices[i] = new Vector3(Body.VerticesData[dataIdx], Body.VerticesData[dataIdx + 1],
            //        Body.VerticesData[dataIdx + 2]);
        }

        private void ParseNormalsData()
        {
            Normals = new Vector3[Body.NormalsData.Length / 3];

            for (int i = 0, dataIdx = 0; i < Normals.Length; ++i, dataIdx += 3)
                Normals[i] = new Vector3(Body.NormalsData[dataIdx], Body.NormalsData[dataIdx + 1],
                    Body.NormalsData[dataIdx + 2]);
        }

        private void ParseIndicesData()
        {
            if (UsingShortIndices)
                IndicesS = Body.IndicesDataS;
            else
                Indices = Body.IndicesData;
        }

        private void ParseUvsData()
        {
            Uvs = Body.UvsData;
            //Uvs = new Vector2[Body.UvsData.Length / 2];
            //for (int i = 0, dataIdx = 0; i < Uvs.Length; ++i, dataIdx += 2)
            //    Uvs[i] = new Vector2(Body.UvsData[dataIdx], Body.UvsData[dataIdx + 1]);
        }

        private void ParseTextureData()
        {
            TextureData = Body.TextureData;
        }

#endregion

#region UTIL

        /// <summary>
        ///     Reset this <see cref="T:VOLSFrame" />.
        /// </summary>
        public void Reset(string newPath)
        {
            Initialize(newPath);
            //ReadBodyNumber(0);
        }

        private void ConfigBodyReader()
        {
            if (Version > 12)
                Body.SetConfigOption(VologramBodyReader.ConfigOption.BackwardsEnabled, true);
        }

        private bool IsMovingBackwards(int from, int to)
        {
            from = from == -1 ? 0 : from;
            int distForward, distBackwards;

            if (from > to)
            {
                distForward = LastFrame - from + to;
                distBackwards = from - to;
            }
            else
            {
                distForward = to - from;
                distBackwards = from + LastFrame - to;
            }

            return distForward > distBackwards;
        }

#endregion
    }
}

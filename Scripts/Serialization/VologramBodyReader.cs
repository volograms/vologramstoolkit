﻿// Copyright (C) 2018 Volograms Limited. All rights reserved.
//
// This file is part of Volograms Toolkit SDK.
//
// See LICENSE in the project root for license information.

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using Unity.Collections;

namespace VologramsToolkit.Scripts.Serialization
{
    /// <summary>
    /// Class representing data in the sequence.vols files.
    /// </summary>
    public class VologramBodyReader
    {
        public int FrameNumber { get; private set; }
        public int LastKeyFrameNumber { get; private set; }
        private int _previousFrame = -1;

        public int MeshDataSize { get; private set; }

        public byte Keyframe { get; private set; }

        private int _verticesSize;
        //public float[] VerticesData { get; private set; }
        public Vector3[] VerticesData { get; private set; }

        private int _indicesSize;
        public int[] IndicesData { get; private set; }
        public ushort[] IndicesDataS { get; private set; }

        private int _uvsSize;
        //public float[] UvsData { get; private set; }
        public Vector2[] UvsData { get; private set; }

        private int _frameMeshDataSize;

        public bool UsingShortIndices { get; private set; }

        private int _normalsSize;
        public float[] NormalsData { get; private set; }

        private int _textureSize;
        public byte[] TextureData { get; private set; }

        public int Version { get; }

        //public string Filename { get; private set; } = string.Empty;

        //long fileLength;
        private long _filePosition;
        public long FrameStartPosition { get; private set; }
        public int FrameStartFile { get; private set; }

        private struct FileDetails
        {
            public readonly string FileName;
            public readonly long FileLength;

            public FileDetails(string fileName, long fileLength)
            {
                FileName = fileName;
                FileLength = fileLength;
            }
        }

        private readonly List<FileDetails> _fileDetails;
        public int ActiveFile { get; private set; }

        public int FileCount => _fileDetails.Count;

        public enum ConfigOption : uint
        {
            BackwardsEnabled = 0x1
        }

        private uint _config;

        public bool EoCurrentF => _filePosition == _fileDetails[ActiveFile].FileLength;

        public bool SoCurrentF => _filePosition == 0;

        private readonly WebClient _webClient;
        public bool DownloaderMode { get; set; }

        private bool _useStreamingAssets;

        public VologramBodyReader(bool useStreamingAssets = false, int version = 12, params string[] files)
        {
            FrameNumber = -1;
            LastKeyFrameNumber = -1;
            //VerticesData = new float[0];
            VerticesData = new Vector3[0];
            NormalsData = new float[0];
            IndicesData = new int[0];
            IndicesDataS = new ushort[0];
            //UvsData = new float[0];
            UvsData = new Vector2[0];
            TextureData = new byte[0];
            Version = version;
            _useStreamingAssets = useStreamingAssets;

            _webClient = new WebClient();

            _fileDetails = new List<FileDetails>(files.Length);

            if (files.Length > 0)
                DownloaderMode = Uri.TryCreate(files[0], UriKind.Absolute, out Uri uriTest) &&
                                 (uriTest.Scheme == Uri.UriSchemeHttp || uriTest.Scheme == Uri.UriSchemeHttps);

            foreach (string f in files)
                try
                {
#if UNITY_ANDROID && !UNITY_EDITOR
                    using (Stream stream = DownloaderMode ? _webClient.OpenRead(f) : !_useStreamingAssets ? File.OpenRead(f) : BetterStreamingAssets.OpenRead(f))
#else
                    using (Stream stream = DownloaderMode ? _webClient.OpenRead(f) : File.OpenRead(f))
#endif
                    using (BinaryReader reader = new BinaryReader(stream ?? throw new ArgumentNullException(nameof(stream))))
                    {
                        long fileLength = reader.BaseStream.Length;
                        _filePosition = 0;
                        _fileDetails.Add(new FileDetails(f, fileLength));
                    }
                }
                catch { Reset(); throw; }
        }

        public void Refresh(params string[] files)
        {
            _fileDetails.Clear();

            if (files.Length > 0)
                DownloaderMode = Uri.TryCreate(files[0], UriKind.Absolute, out Uri uriTest) &&
                                 (uriTest.Scheme == Uri.UriSchemeHttp || uriTest.Scheme == Uri.UriSchemeHttps);

            foreach (string f in files)
                try
                {
#if UNITY_ANDROID && !UNITY_EDITOR
                    using (Stream stream = DownloaderMode ? _webClient.OpenRead(f) : !_useStreamingAssets ? File.OpenRead(f) : BetterStreamingAssets.OpenRead(f))
#else
                    using (Stream stream = DownloaderMode ? _webClient.OpenRead(f) : File.OpenRead(f))
#endif
                    using (BinaryReader reader = new BinaryReader(stream ?? throw new ArgumentNullException(nameof(stream))))
                    {
                        long fileLength = reader.BaseStream.Length;
                        _filePosition = 0;
                        _fileDetails.Add(new FileDetails(f, fileLength));
                    }
                }
                catch { Reset(); throw; }
        }

        #region READING
        /// <summary>
        /// Reads the data when the version number >= 11.
        /// </summary>
        /// <param name="hasNormals">If set to <c>true</c> has normals</param>
        /// <param name="textured">If set to <c>true</c> is textured</param>
        public void ReadV11(bool hasNormals, bool textured)
        {
            try
            {
                if (_fileDetails == null || _fileDetails.Count == 0)
                    throw GenerateException("Body reader is not initialised");

                string filename = _fileDetails[ActiveFile].FileName;

#if UNITY_ANDROID && !UNITY_EDITOR
                using (Stream stream = DownloaderMode ? _webClient.OpenRead(filename) : !_useStreamingAssets ? File.OpenRead(filename) : BetterStreamingAssets.OpenRead(filename))
#else
                using (Stream stream = DownloaderMode ? _webClient.OpenRead(filename) : File.OpenRead(filename))
#endif
                using (BinaryReader reader = new BinaryReader(stream ?? throw new ArgumentNullException(nameof(stream))))
                {
                    stream.Position = _filePosition;
                    FrameStartPosition = _filePosition;
                    FrameStartFile = ActiveFile;

                    FrameNumber = reader.ReadInt32();
                    MeshDataSize = reader.ReadInt32();
                    Keyframe = reader.ReadByte();

                    FrameHeaderCheck();

                    if (_previousFrame + 1 != FrameNumber && FrameNumber != 0) throw GenerateException("Expected frame number " + (_previousFrame + 1) + " but was " + FrameNumber);

                    // Read frame data
                    ReadFrameData(reader, hasNormals, textured);
                    _filePosition = stream.Position;

                    _previousFrame = FrameNumber;

                    if (EoCurrentF)
                        MoveToNextFile();
                }
            }
            catch (InvalidVolsBodyException) { throw; }
            catch { Reset(); throw; }
        }

        private void ReadFrameData(BinaryReader reader, bool hasNormals, bool textured)
        {
            ReadVerticesData(reader);

            if (hasNormals)
                ReadNormalsData(reader);
            else
                NormalsData = new float[0];

            if (Keyframe == 0x1 || Keyframe == 0x2)
            {
                LastKeyFrameNumber = FrameNumber;
                ReadIndicesData(reader);
                ReadUvsData(reader);
            }

            if (textured)
                ReadTextureData(reader);
            else
                TextureData = new byte[0];

            // Read frame size (at the end)
            _frameMeshDataSize = reader.ReadInt32();
            FrameDataSizeCheck();
        }
#endregion

#region SEEKING_FORWARD
        /// <summary>
        /// Seeks the data of a specified frame when the version number >= 11
        /// </summary>
        /// <param name="hasNormals">If set to <c>true</c> has normals</param>
        /// <param name="textured">If set to <c>true</c> is textured</param>
        /// <param name="readFrame">Index of frame to be read</param>
        /// <param name="isMovingBackwards">If set to <c>true</c> is skipping backwards</param>
        public void SeekV11(bool hasNormals, bool textured, int readFrame, bool isMovingBackwards)
        {
            try
            {
                if (readFrame < 0) throw new ArgumentException("Value cannot be negative", nameof(readFrame));

                if (Version < 12 && IsConfigOptionSet(ConfigOption.BackwardsEnabled))
                {
                    SetConfigOption(ConfigOption.BackwardsEnabled, false);
                    Debug.LogWarning("Backward traversal is not compatible with version " + Version + ". Turning it off.");
                }

                if (isMovingBackwards && IsConfigOptionSet(ConfigOption.BackwardsEnabled))
                    SeekBackward(hasNormals, textured, readFrame);
                else
                    SeekForward(hasNormals, textured, readFrame);

            }
            catch (InvalidVolsBodyException) { throw; }
            catch { Reset(); throw; }
        }

        private void SeekForward(bool hasNormals, bool textured, int readFrame)
        {
            int frameWhenStarted = FrameNumber;

            while (FrameNumber != readFrame)
            {
                string filename = _fileDetails[ActiveFile].FileName;

#if UNITY_ANDROID && !UNITY_EDITOR
                using (Stream stream = DownloaderMode ? _webClient.OpenRead(filename) : !_useStreamingAssets ? File.OpenRead(filename) : BetterStreamingAssets.OpenRead(filename))
#else
                using (Stream stream = DownloaderMode ? _webClient.OpenRead(filename) : File.OpenRead(filename))
#endif
                using (BinaryReader reader = new BinaryReader(stream ?? throw new ArgumentNullException(nameof(stream))))
                {
                    stream.Position = _filePosition;
                    FrameStartPosition = _filePosition;
                    FrameStartFile = ActiveFile;

                    FrameNumber = reader.ReadInt32();
                    MeshDataSize = reader.ReadInt32();
                    Keyframe = reader.ReadByte();

                    if (_previousFrame + 1 != FrameNumber && FrameNumber != 0) throw GenerateException("Expected frame number " + (_previousFrame + 1) + " during skip but was " + FrameNumber);

                    FrameHeaderCheck();

                    if (FrameNumber == frameWhenStarted) throw new ArgumentException("Frame number does not exist", nameof(readFrame));

                    if (FrameNumber == readFrame)
                    {
                        // Read frame data
                        ReadFrameData(reader, hasNormals, textured);
                    }
                    else if (Keyframe == 0x1 || Keyframe == 0x2)
                    {
                        LastKeyFrameNumber = FrameNumber;
                        // Skip key frame data
                        SkipFrameData(reader, hasNormals, textured);
                    }
                    else
                    {
                        reader.BaseStream.Seek(MeshDataSize, SeekOrigin.Current);
                        _frameMeshDataSize = reader.ReadInt32();
                        FrameDataSizeCheck();
                    }
                    _filePosition = reader.BaseStream.Position;
                    _previousFrame = FrameNumber;

                    if (EoCurrentF)
                    {
                        MoveToNextFile();
                        reader.BaseStream.Position = _filePosition;
                    }

                    // If skip is called before read, frameWhenStarted will be -1
                    // This will lead to an infinite loop
                    // We correct if after frame 0 is read so as not throw an exception immediately
                    if (frameWhenStarted == -1)
                        frameWhenStarted = 0;
                }
            }
        }
#endregion

#region SEEKING_BACKWARDS

        private void SeekBackward(bool hasNormals, bool textured, int readFrame)
        {
            bool frameReached = false;
            bool keyframeReached = false;

            int backSeekFrameNumber = -1;
            int backSeekMeshDataSize = 0;
            byte backSeekKeyFrame = 0x0;
            int backSeekFileIndex = -1;

            long framePosition = 0;
            string filename;

            if (FrameNumber == -1)
            {
                filename = _fileDetails[ActiveFile].FileName;
#if UNITY_ANDROID && !UNITY_EDITOR
                using (Stream stream = DownloaderMode ? _webClient.OpenRead(filename) : !_useStreamingAssets ? File.OpenRead(filename) : BetterStreamingAssets.OpenRead(filename))
#else
                using (Stream stream = DownloaderMode ? _webClient.OpenRead(filename) : File.OpenRead(filename))
#endif
                using (BinaryReader reader = new BinaryReader(stream ?? throw new ArgumentNullException(nameof(stream))))
                {
                    //stream.Position = filePosition;

                    FrameNumber = reader.ReadInt32();
                    MeshDataSize = reader.ReadInt32();
                    Keyframe = reader.ReadByte();
                    ReadFrameData(reader, hasNormals, textured);

                    _filePosition = stream.Position;
                }

                if (readFrame == 0) return;
            }

            SkipBackFrame();
            SkipBackFrame();
            int frameWhenStarted = FrameNumber;

            while (!keyframeReached)
            {
                filename = _fileDetails[ActiveFile].FileName;
#if UNITY_ANDROID && !UNITY_EDITOR
                using (Stream stream = DownloaderMode ? _webClient.OpenRead(filename) : !_useStreamingAssets ? File.OpenRead(filename) : BetterStreamingAssets.OpenRead(filename))
#else
                using (Stream stream = DownloaderMode ? _webClient.OpenRead(filename) : File.OpenRead(filename))
#endif
                using (BinaryReader reader = new BinaryReader(stream ?? throw new ArgumentNullException(nameof(stream))))
                {
                    reader.BaseStream.Position = _filePosition;

                    FrameNumber = reader.ReadInt32();
                    MeshDataSize = reader.ReadInt32();
                    Keyframe = reader.ReadByte();

                    if (FrameNumber != _previousFrame - 1 && _previousFrame > 0)
                        throw GenerateException("Expected frame number " +
                                                (_previousFrame - 1) + " during back skip but was " +
                                                FrameNumber + " instead");

                    FrameHeaderCheck();

                    if (!frameReached && frameWhenStarted == FrameNumber ||
                        frameReached && FrameNumber == 0 && Keyframe == 0)
                        throw new ArgumentException("Frame number does not exist", nameof(readFrame));

                    _previousFrame = FrameNumber;

                    if (FrameNumber == readFrame)
                    {
                        // Save the position of the desired frame (except the
                        // nine-byte header) for later
                        framePosition = reader.BaseStream.Position;

                        frameReached = true;

                        // Save the details of the frame for later
                        // The key frame could be in a different file to the
                        // desired frame
                        backSeekFrameNumber = FrameNumber;
                        backSeekMeshDataSize = MeshDataSize;
                        backSeekKeyFrame = Keyframe;
                        backSeekFileIndex = ActiveFile;

                        if (Keyframe == 0x1 || Keyframe == 0x2)
                        {
                            keyframeReached = true;
                            LastKeyFrameNumber = FrameNumber;
                        }

                        reader.BaseStream.Seek(-9, SeekOrigin.Current);
                    }
                    else if (Keyframe == 0x1 || Keyframe == 0x2)
                    {
                        if (frameReached)
                        {
                            keyframeReached = true;
                            LastKeyFrameNumber = FrameNumber;

                            // Read keyframe data
                            ReadFrameData(reader, hasNormals, textured);
                        }
                        else
                        {
                            reader.BaseStream.Seek(-9, SeekOrigin.Current);
                        }
                    }
                    else
                    {
                        reader.BaseStream.Seek(-9, SeekOrigin.Current);
                    }

                    _filePosition = reader.BaseStream.Position;
                }

                SkipBackFrame();
            }

            // Take the saved data to find the desired frame
            FrameNumber = backSeekFrameNumber;
            MeshDataSize = backSeekMeshDataSize;
            Keyframe = backSeekKeyFrame;
            ActiveFile = backSeekFileIndex;

            filename = _fileDetails[ActiveFile].FileName;
#if UNITY_ANDROID && !UNITY_EDITOR
            using (Stream stream = DownloaderMode ? _webClient.OpenRead(filename) : !_useStreamingAssets ? File.OpenRead(filename) : BetterStreamingAssets.OpenRead(filename))
#else
            using (Stream stream = DownloaderMode ? _webClient.OpenRead(filename) : File.OpenRead(filename))
#endif
            using (BinaryReader reader = new BinaryReader(stream ?? throw new ArgumentNullException(nameof(stream))))
            {
                reader.BaseStream.Position = framePosition;
                FrameStartPosition = _filePosition;
                FrameStartFile = ActiveFile;

                ReadFrameData(reader, hasNormals, textured);

                _filePosition = reader.BaseStream.Position;
            }

            _previousFrame = FrameNumber;

            if (EoCurrentF)
                MoveToNextFile();
        }

        private void SkipFrameData(BinaryReader reader, bool hasNormals, bool textured)
        {
            SkipVerticesData(reader);

            if (hasNormals)
                SkipNormalsData(reader);

            ReadIndicesData(reader);
            ReadUvsData(reader);

            if (textured)
                SkipTextureData(reader);

            _frameMeshDataSize = reader.ReadInt32();
            _filePosition = reader.BaseStream.Position;
        }

        // returns whether the reader looped to the start
        private void SkipBackFrame()
        {
            if (SoCurrentF) MoveToPreviousFile();

            string filename = _fileDetails[ActiveFile].FileName;
#if UNITY_ANDROID && !UNITY_EDITOR
            using (Stream stream = DownloaderMode ? _webClient.OpenRead(filename) : !_useStreamingAssets ? File.OpenRead(filename) : BetterStreamingAssets.OpenRead(filename))
#else
            using (Stream stream = DownloaderMode ? _webClient.OpenRead(filename) : File.OpenRead(filename))
#endif
            using (BinaryReader reader = new BinaryReader(stream ?? throw new ArgumentNullException(nameof(stream))))
            {
                reader.BaseStream.Position = _filePosition;

                reader.BaseStream.Seek(-4, SeekOrigin.Current);
                _frameMeshDataSize = reader.ReadInt32();
                reader.BaseStream.Seek(-_frameMeshDataSize, SeekOrigin.Current);
                reader.BaseStream.Seek(-13, SeekOrigin.Current);

                _filePosition = reader.BaseStream.Position;
            }
        }
#endregion

#region MESH_COMPONENTS

        private void ReadVerticesData(BinaryReader reader)
        {
            _verticesSize = reader.ReadInt32();

            if (_verticesSize <= 0) throw GenerateException("Invalid vertices length value (" + _verticesSize + ")");

#if !UNITY_2017_3_OR_NEWER
            if (verticesSize / (sizeof(float) * 3) > 65536)
            {
                throw GenerateException("Vertices length is greater than the Unity limit of 65536");
            }
#endif
            //VerticesData = new float[_verticesSize / sizeof(float)];

            byte[] data = new byte[_verticesSize];
            reader.Read(data, 0, _verticesSize);
            using (NativeArray<byte> nByteArray = new NativeArray<byte>(data, Allocator.Persistent))
            {
                VerticesData = nByteArray.Reinterpret<Vector3>(1).ToArray();
            }

            //Buffer.BlockCopy(data, 0, VerticesData, 0, data.Length);
        }

        private void ReadNormalsData(BinaryReader reader)
        {
            _normalsSize = reader.ReadInt32();

            if (_normalsSize <= 0) throw GenerateException("Invalid normals length value (" + _normalsSize + ")");

            if (_normalsSize != _verticesSize) throw GenerateException("The number of normals does not match the number of vertices");

            NormalsData = new float[_normalsSize / sizeof(float)];
            byte[] data = new byte[_normalsSize];
            reader.Read(data, 0, _normalsSize);
            Buffer.BlockCopy(data, 0, NormalsData, 0, data.Length);
        }

        private void ReadIndicesData(BinaryReader reader)
        {
            _indicesSize = reader.ReadInt32();

            if (_indicesSize <= 0) throw GenerateException("Invalid indices length value (" + _indicesSize + ")");

            int verticesCount = _verticesSize / sizeof(float);
            if (verticesCount / 3 < ushort.MaxValue)
            {
                UsingShortIndices = true;
                IndicesData = new int[_indicesSize / sizeof(short)];

                byte[] data = new byte[_indicesSize];
                reader.Read(data, 0, _indicesSize);

                using (NativeArray<byte> nByteArray = new NativeArray<byte>(data, Allocator.Persistent))
                {
                    IndicesDataS = nByteArray.Reinterpret<ushort>(1).ToArray();
                }

                // We can't use Buffer.BlockCopy here!
                //for (int i = 0; i < _indicesSize / sizeof(short); i++) IndicesData[i] = BitConverter.ToUInt16(data, i * sizeof(short));
            }
            else
            {
                UsingShortIndices = false;
                IndicesData = new int[_indicesSize / sizeof(int)];

                byte[] data = new byte[_indicesSize];
                reader.Read(data, 0, _indicesSize);
                Buffer.BlockCopy(data, 0, IndicesData, 0, _indicesSize);
            }
        }

        private void ReadUvsData(BinaryReader reader)
        {
            _uvsSize = reader.ReadInt32();

            if (_uvsSize <= 0) throw GenerateException("Invalid uvs length value (" + _uvsSize + ")");

            if (_uvsSize / 2 != _verticesSize / 3) throw GenerateException("The number of UVs does not match the number of vertices");

            //UvsData = new float[_uvsSize / sizeof(float)];

            byte[] data = new byte[_uvsSize];
            reader.Read(data, 0, _uvsSize);
            using (NativeArray<byte> nByteArray = new NativeArray<byte>(data, Allocator.Persistent))
            {
                UvsData = nByteArray.Reinterpret<Vector2>(1).ToArray();
            }
            //Buffer.BlockCopy(data, 0, UvsData, 0, _uvsSize);
        }

        private void ReadTextureData(BinaryReader reader)
        {
            _textureSize = reader.ReadInt32();

            if (_textureSize <= 0) throw GenerateException("Invalid texture size value (" + _textureSize + ")");

            TextureData = new byte[_textureSize];
            reader.Read(TextureData, 0, _textureSize);
        }

        private void SkipVerticesData(BinaryReader reader)
        {
            _verticesSize = reader.ReadInt32();

            if (_verticesSize <= 0) throw GenerateException("Invalid vertices length value (" + _verticesSize + ")");

            reader.BaseStream.Seek(_verticesSize, SeekOrigin.Current);
        }

        private void SkipNormalsData(BinaryReader reader)
        {
            _normalsSize = reader.ReadInt32();

            if (_normalsSize <= 0) throw GenerateException("Invalid normals length value (" + _normalsSize + ")");

            reader.BaseStream.Seek(_normalsSize, SeekOrigin.Current);
        }

        private void SkipTextureData(BinaryReader reader)
        {
            _textureSize = reader.ReadInt32();

            if (_textureSize <= 0) throw GenerateException("Invalid texture size value (" + _textureSize + ")");

            reader.BaseStream.Seek(_textureSize, SeekOrigin.Current);
        }
#endregion

#region UTILS

        private void Reset()
        {
            _fileDetails.Clear();
            _filePosition = 0;

            FrameNumber = -1;
            LastKeyFrameNumber = -1;
            Keyframe = 0;
            UsingShortIndices = false;
            //VerticesData = new float[0];
            VerticesData = new Vector3[0];
            NormalsData = new float[0];
            IndicesData = new int[0];
            IndicesDataS = new ushort[0];
            //UvsData = new float[0];
            UvsData = new Vector2[0];
            TextureData = new byte[0];
        }

        private void FrameHeaderCheck()
        {
            if (FrameNumber < 0) throw GenerateException("Invalid frame number (" + FrameNumber + ")");

            if (MeshDataSize < 1) throw GenerateException("Invalid mesh data size (" + MeshDataSize + ")");

            if (Keyframe > 2) throw GenerateException("Invalid key frame value (" + Keyframe + ")");
        }

        private void FrameDataSizeCheck()
        {
            if (_frameMeshDataSize != MeshDataSize)
                throw GenerateException("Total size before (" +
                                        MeshDataSize + ") and after (" +
                                        _frameMeshDataSize + ") body do not match");
        }

        private InvalidVolsBodyException GenerateException(string message)
        {
            Reset();
            return new InvalidVolsBodyException(message);
        }

        public void SetConfigOption(ConfigOption option, bool set)
        {
            if (set)
                _config |= (uint)option;
            else
                _config &= ~(uint)option;
        }

        public bool IsConfigOptionSet(ConfigOption option)
        {
            uint check = _config & (uint)option;
            return check != 0x0;
        }

        private void MoveToNextFile()
        {
            ActiveFile = (ActiveFile + 1) % _fileDetails.Count;
            _filePosition = 0;
        }

        private void MoveToPreviousFile()
        {
            ActiveFile = (ActiveFile - 1) % _fileDetails.Count;
            if (ActiveFile < 0)
                ActiveFile += _fileDetails.Count;


            _filePosition = _fileDetails[ActiveFile].FileLength;
        }

#endregion
    }
}

﻿// Copyright (C) 2018 Volograms Limited. All rights reserved.
//
// This file is part of Volograms Toolkit SDK.
//
// See LICENSE in the project root for license information.

using System;
using UnityEngine;
using UnityEngine.Rendering;

namespace VologramsToolkit.Scripts.Serialization
{
    /// <summary>
    ///     Class for storing important vologram mesh data
    /// </summary>
    public class VologramMesh
    {
        private static readonly int MainTexPropertyId = Shader.PropertyToID("_VMainTex");
        public int Version { get; }
        public Vector3[] Vertices { get; }
        public bool HasNormals { get; private set; }
        public Vector3[] Normals { get; private set; }
        public Vector2[] Uvs { get; private set; }
        public Color[] Colors { get; private set; }
        public int[] Indices { get; private set; }
        public ushort[] IndicesS { get; private set; }
        public MeshTopology MeshTopology { get; }
        public bool IsTextured { get; private set; }
        public ushort TextureWidth { get; private set; }
        public ushort TextureHeight { get; private set; }
        public TextureFormat TextureFormat { get; private set; }
        public byte[] TextureData { get; private set; }
        public string FrameName { get; }
        public bool KeyFrame { get; private set; }
        public bool UsingShortIndices { get; }
        public bool HasColors { get; private set; }
        public int Id { get; }

        public VologramMesh(int id, int version, Vector3[] vertices, string frameName, MeshTopology meshTopology)
        {
            if (!Enum.IsDefined(typeof(MeshTopology), meshTopology))
                throw new ArgumentException("Not a valid UnityEngine.MeshTopology", nameof(meshTopology));

            Vertices = vertices ?? throw new ArgumentNullException(nameof(vertices));
            Id = id;
            Version = version;
            MeshTopology = meshTopology;
            FrameName = frameName;
            UsingShortIndices = vertices.Length <= ushort.MaxValue;
            ValidityCheck();
        }

        public void AddKeyFrameData(Vector2[] uvs, int[] indices)
        {
            Uvs = uvs ?? throw new ArgumentNullException(nameof(uvs));
            Indices = indices ?? throw new ArgumentNullException(nameof(indices));
            KeyFrame = true;
            ValidityCheck();
        }

        public void AddKeyFrameData(Vector2[] uvs, ushort[] indices)
        {
            Uvs = uvs ?? throw new ArgumentNullException(nameof(uvs));
            IndicesS = indices ?? throw new ArgumentNullException(nameof(indices));
            KeyFrame = true;
            ValidityCheck();
        }

        public void AddNormals(Vector3[] normals)
        {
            Normals = normals ?? throw new ArgumentNullException(nameof(normals));
            HasNormals = true;
            ValidityCheck();
        }

        public void AddTextureData(ushort textureWidth, ushort textureHeight, byte[] textureData,
            TextureFormat textureFormat)
        {
            TextureData = textureData ?? throw new ArgumentNullException(nameof(textureData));
            TextureWidth = textureWidth;
            TextureHeight = textureHeight;
            TextureFormat = textureFormat;
            IsTextured = true;
            ValidityCheck();
        }

        public void AddColorData(Color[] colors)
        {
            Colors = colors ?? throw new ArgumentNullException(nameof(colors));
            HasColors = true;
            ValidityCheck();
        }

        public void UpdateMesh(ref Mesh mesh)
        {
            if (KeyFrame)
                mesh.Clear();

            mesh.indexFormat = UsingShortIndices ? IndexFormat.UInt16 : IndexFormat.UInt32;

            mesh.vertices = Vertices;

            if (HasNormals)
                mesh.normals = Normals;

            if (KeyFrame)
            {
                mesh.uv = Uvs;
                if (UsingShortIndices)
                    mesh.SetIndices(IndicesS, MeshTopology, 0);
                else
                    mesh.SetIndices(Indices, MeshTopology, 0);
            }

            if (HasColors)
                mesh.colors = Colors;
        }

        public void UpdateTexture(ref Material material)
        {
            if (!IsTextured)
                return;

            Texture2D tex = new Texture2D(TextureWidth, TextureHeight, TextureFormat, false);
            tex.LoadRawTextureData(TextureData);
            tex.Apply();
            material.SetTexture(MainTexPropertyId, tex);
        }

        private void ValidityCheck()
        {
            if (Version < 0)
                throw new InvalidVolsMeshException("Invalid value for Version (" + Version + ")");

            if (Vertices == null || Vertices.Length == 0)
                throw new InvalidVolsMeshException("Vertices cannot be null or empty");

            if (HasNormals && Normals.Length != Vertices.Length)
                throw new InvalidVolsMeshException("Normals (" + Normals.Length +
                                                   ") must be the same length as vertices (" + Vertices.Length + ")");

            if (KeyFrame && Uvs.Length != Vertices.Length)
                throw new InvalidVolsMeshException("Uvs (" + Uvs.Length +
                                                   ") must be the same length as vertices (" + Vertices.Length + ")");

            if (KeyFrame && ((Indices != null && Indices.Length == 0) || (IndicesS != null && IndicesS.Length == 0)))
                throw new InvalidVolsMeshException("Indices cannot be an empty array");

            if (IsTextured && TextureWidth == 0)
                throw new InvalidVolsMeshException("Invalid value for TextureWidth (" + TextureWidth + ")");

            if (IsTextured && TextureHeight == 0)
                throw new InvalidVolsMeshException("Invalid value for TextureHeight (" + TextureHeight + ")");

            if (IsTextured && TextureData.Length == 0)
                throw new InvalidVolsMeshException("TextureData cannot be empty");

            if (HasColors && Colors.Length != Vertices.Length)
                throw new InvalidVolsMeshException("Colors (" + Colors.Length +
                                                   ") must be the same length as vertices (" + Vertices.Length + ")");
        }
    }
}
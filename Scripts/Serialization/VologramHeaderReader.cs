﻿// Copyright (C) 2018 Volograms Limited. All rights reserved.
//
// This file is part of Volograms Toolkit SDK.
//
// See LICENSE in the project root for license information.

using System;
using System.IO;
using System.Net;
using UnityEngine;

namespace VologramsToolkit.Scripts.Serialization
{
    /// <summary>
    ///     Class representing data in the header.vols file.
    /// </summary>
    public class VologramHeaderReader
    {
        // Version 1.0
        public string Format { get; }
        public int Version { get; }
        public int Compression { get; }
        public string MeshName { get; }
        public string Material { get; }
        public string Shader { get; }
        public int Topology { get; }
        public int FrameCount { get; }

        // Added in Version 1.1
        public byte Normals { get; }
        public byte Textured { get; }
        public ushort TextureWidth { get; }
        public ushort TextureHeight { get; }
        public ushort TextureFormat { get; }

        // Added in Version 1.2
        public float[] Translation { get; }
        public float[] Rotation { get; }
        public float Scale { get; }

        public string Filename { get; }

        public bool DownloaderMode { get; }

        private bool _useStreamingAssets = false;

        public VologramHeaderReader(string filename)
        {
            WebClient webClient = new WebClient();

            DownloaderMode = Uri.TryCreate(filename, UriKind.Absolute, out Uri uriTest) &&
                             (uriTest.Scheme == Uri.UriSchemeHttp || uriTest.Scheme == Uri.UriSchemeHttps);
            
#if UNITY_ANDROID && !UNITY_EDITOR
            _useStreamingAssets = filename.StartsWith(Application.streamingAssetsPath);
            using (Stream stream = DownloaderMode ? webClient.OpenRead(filename) : !_useStreamingAssets ? File.OpenRead(filename) : BetterStreamingAssets.OpenRead(filename))
#else
            using (Stream stream = DownloaderMode ? webClient.OpenRead(filename) : File.OpenRead(filename))
#endif
            using (BinaryReader reader = new BinaryReader(stream ?? throw new ArgumentNullException(nameof(stream))))
            {
                Filename = filename;

                Format = reader.ReadString();
                Version = reader.ReadInt32();
                Compression = reader.ReadInt32();
                MeshName = reader.ReadString();
                Material = reader.ReadString();
                Shader = reader.ReadString();
                Topology = reader.ReadInt32();
                FrameCount = reader.ReadInt32();

                Normals = 0;
                Textured = 0;
                TextureWidth = 0;
                TextureHeight = 0;
                TextureFormat = 0;

                Translation = new[] { 0f, 0f, 0f };
                Rotation = new[] { 0f, 0f, 0f, 1f };
                Scale = 1f;

                if (Version >= 11)
                {
                    Normals = reader.ReadByte();
                    Textured = reader.ReadByte();
                    TextureWidth = reader.ReadUInt16();
                    TextureHeight = reader.ReadUInt16();
                    TextureFormat = reader.ReadUInt16();
                    if (Version >= 12)
                    {
                        // Translation
                        Translation = new float[3];
                        Translation[0] = reader.ReadSingle();
                        Translation[1] = reader.ReadSingle();
                        Translation[2] = reader.ReadSingle();

                        //Rotation
                        Rotation = new float[4];
                        Rotation[0] = reader.ReadSingle();
                        Rotation[1] = reader.ReadSingle();
                        Rotation[2] = reader.ReadSingle();
                        Rotation[3] = reader.ReadSingle();

                        // Scale
                        Scale = reader.ReadSingle();
                    }
                }
            }

            VerifyHeader();
        }

        private void VerifyHeader()
        {
            if (string.IsNullOrEmpty(Format))
                throw GenerateException(EmptyMessage(nameof(Format)));

            if (!string.Equals(Format, "VOLS") && !string.Equals(Format, "VOL"))
                throw GenerateException(InvalidMessage(nameof(Format), Format));

            if (Version < 0)
                throw GenerateException(InvalidMessage(nameof(Version), Version));

            if (Compression < 0)
                throw GenerateException(InvalidMessage(nameof(Compression), Compression));

            if (string.IsNullOrEmpty(MeshName))
                throw GenerateException(EmptyMessage(nameof(MeshName)));

            if (string.IsNullOrEmpty(Material))
                throw GenerateException(EmptyMessage(nameof(Material)));

            if (string.IsNullOrEmpty(Shader))
                throw GenerateException(EmptyMessage(nameof(Shader)));

            if (!Enum.IsDefined(typeof(MeshTopology), (MeshTopology)Topology))
                throw GenerateException(InvalidMessage(nameof(Topology), Topology));

            if (FrameCount < 0)
                throw GenerateException(InvalidMessage(nameof(FrameCount), FrameCount));

            if (Version < 11)
                return;
            // The following don't need to be checked if the Version number is less than 11

            if (Normals > 0x1)
                throw GenerateException(InvalidMessage(nameof(Normals), Normals));

            if (Textured > 0x1)
                throw GenerateException(InvalidMessage(nameof(Textured), Textured));

            if (Textured == 0x1 && !IsPowerOfTwo(TextureWidth))
                throw GenerateException(InvalidMessage(nameof(TextureWidth), TextureWidth));

            if (Textured == 0x1 && !IsPowerOfTwo(TextureHeight))
                throw GenerateException(InvalidMessage(nameof(TextureHeight), TextureHeight));

            if (Textured == 0x1 && !Enum.IsDefined(typeof(TextureFormat), (TextureFormat)TextureFormat))
                throw GenerateException(InvalidMessage(nameof(TextureFormat), TextureFormat));

            if (Version < 12)
                return;
            // The following don't need to be checked if the Version number is less than 12

            if (Translation.Length == 0)
                throw GenerateException(EmptyMessage(nameof(Translation)));

            if (Rotation.Length == 0)
                throw GenerateException(EmptyMessage(nameof(Rotation)));

            if (Scale <= 0f)
                throw GenerateException(InvalidMessage(nameof(Scale), Scale));
        }

        private static string InvalidMessage(string name, object value)
        {
            return "Value of " + name + " cannot be " + value;
        }

        private static string EmptyMessage(string name)
        {
            return name + " cannot be null or empty";
        }

        private InvalidVolsHeaderException GenerateException(string message)
        {
            return new InvalidVolsHeaderException(message);
        }

        private static bool IsPowerOfTwo(ushort num)
        {
            return num != 0 && (num & (num - 1)) == 0;
        }

        public VologramsHeader GetHeader()
        {
            return new VologramsHeader
            {
                Format = Format,
                Version = Version,
                Compression = Compression,
                MeshName = MeshName,
                Material = Material,
                Shader = Shader,
                Topology = Topology,
                TotalFrameCount = FrameCount,
                HasNormals = Normals == 0x1,
                HasTexture = Textured == 0x1,
                TextureWidth = TextureWidth,
                TextureHeight = TextureHeight,
                Translation = Translation,
                Rotation = Rotation,
                Scale = Scale
            };
        }

        public override string ToString()
        {
            return "Format: " + Format + "\n" +
                "Version: " + Version + "\n" +
                "Compression: " + Compression + "\n" +
                "Mesh Name: " + MeshName + "\n" +
                "Material: " + Material + "\n" +
                "Shader: " + Shader + "\n" +
                "Topology: " + Topology + "\n" +
                "Frame Count: " + FrameCount + "\n" +
                "Normals: " + Normals + "\n" +
                "Textured: " + Textured + "\n" +
                "Texture Width: " + TextureWidth + "\n" +
                "Texture Height: " + TextureHeight + "\n" +
                "Texture Format: " + TextureFormat + "\n" +
                "Translation: " + string.Join(", ", Translation) + "\n" +
                "Rotation: " + string.Join(", ", Rotation) + "\n" +
                "Scale: " + Scale;
        }
    }

    public struct VologramsHeader
    {
        public string Format;
        public int Version;
        public int Compression;
        public string MeshName;
        public string Material;
        public string Shader;
        public int Topology;
        public int TotalFrameCount;
        public bool HasNormals;
        public bool HasTexture;
        public int TextureWidth;
        public int TextureHeight;
        public float[] Translation;
        public float[] Rotation;
        public float Scale;
    }
}

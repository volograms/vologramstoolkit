﻿using System;
using System.Runtime.Serialization;

namespace VologramsToolkit.Scripts.Serialization
{
    [Serializable]
    public class InvalidVolsHeaderException : Exception
    {
        public InvalidVolsHeaderException()
        {
        }

        public InvalidVolsHeaderException(string message) : base(message)
        {
        }

        public InvalidVolsHeaderException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidVolsHeaderException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    [Serializable]
    public class InvalidVolsBodyException : Exception
    {
        public InvalidVolsBodyException()
        {
        }

        public InvalidVolsBodyException(string message) : base(message)
        {
        }

        public InvalidVolsBodyException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidVolsBodyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    [Serializable]
    public class InvalidVolsMeshException : Exception
    {
        public InvalidVolsMeshException()
        {
        }

        public InvalidVolsMeshException(string message) : base(message)
        {
        }

        public InvalidVolsMeshException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidVolsMeshException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
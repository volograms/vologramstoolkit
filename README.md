# VologramsToolkit

## Table of Contents
* [Getting Started](#markdown-header-getting-started)
	+ [Introduction](#markdown-header-introduction)
    + [Before you start: Unity configuration](#markdown-header-before-you-start-unity-configuration)
    + [Choosing the right assets for your application](#markdown-header-choosing-the-right-assets-for-your-application)
    + [Importing Volograms assets into your project](#markdown-header-importing-volograms-assets-into-your-project)
* [Vologram Setup](#markdown-header-vologram-setup)
* [Reference](#markdown-header-reference)
* [Get In Touch](#markdown-header-get-in-touch)
* [Changelog](#markdown-header-changelog)
* [Third-Party Software](#markdown-header-third-party-software)

## Getting Started

### Introduction
To be able to integrate our volograms in your application you need to use our Volograms Unity SDK. This SDK is able to read
and decode our compressed VOL files and render them in real time. The SKD is compatible with desktop, mobile devices, most
VR headsets and also Magic Leap One, you just need to make sure you choose the right assets, as explained below, to maximize
performance on the device.

### Before you start: Unity configuration

Volograms Unity SDK version 1.4 are only compatible with Unity **2019.3** onwards.

### Choosing the right assets for your application

As mentioned above there are different types of assets for different types of platforms. In general, there are two factors
that play an important role on the final performance of the assets: polygonal resolution and texture resolution. Normally, our
assets are either HD (\~40k polygons per frame) or SD (\~20k polygones per frame), and textures are either 2048x2048, or
1024x1024. Besides, there is an extra factor to take into account: VOL files can have the texture images embedded (either in
DXT1 or ETC2 format) or textured can be provided as an mp4 video.

Volograms using video textures perform well on most platforms and take up less space than volograms with embedded images.
In our experience, you can follow the table below as guidance:

| Platform   | Polygon Count | Image Size  |
| :--------- | :-----------: | ----------: |
| PC/Mac     | HD            | 2048 x 2048 |
| iOS        | SD            | 2048 x 2048 |
| Android    | SD            | 1024 x 1024 |
| Magic Leap | SD            | 1024 x 1024 |

If, however, you find that the video-textured vologram does not perform well with your application, then use the volograms
with embedded textures: DXT1 for desktop applications and ETC2 for mobile. This might happen when using an older device
with lower performance.

### Importing Volograms assets into your project

First import the Volograms Unity SDK into your project. This can be done in 2 ways:

- Drag and drop the Unity package file into the project window in Unity, and then click “Import” in the window that appears.
- While Unity is open, click “Assets” in the toolbar, followed by “Import Package” and “Custom Package…”. Then locate the
Unity package file.

Next, open the Assets folder in the project window in Unity and create a new folder. Name this folder “**StreamingAssets**”. Note
that the spelling and the case of the letters are important. This is the folder where most of the volograms will be placed.
Using your computer’s file finder, locate the volograms assets folder. This folder will include a “**header.vols**” file with one or
more “**sequence.vols**” files. The folder may also include a video file which holds the texture and audio of the vologram. Drag
the asset folder into the StreamingAssets folder of your Unity project. Repeat this process for each of the volograms you wish
to import.

## Vologram Setup

### Playing Volograms, step by step (Version 1.5.00 and before)

1. Create an Empty Object in your scene and name it *Vologram*, for example.
2. Create another Empty Object and make it the child of *Vologram*. Name this object Player.
3. Add the VologramsPlayer component to Player.
4. Under Mesh Settings:
	- Drag the vologram asset folder from within the StreamingAssets folder into the box marked “Drag Mesh Folder Here”.
	- Alternatively, manually enter the directory of the vologram asset folder into the text field above the drag-and-drop.
	- You can play from an asset folder outside the StreamingAssets folder by setting "Use full path" to true, and entering the absolute path of the asset folder in the text field.
box. Note that the directory must be relative to the StreamingAssets folder.
5. Under Video Texture Settings:
 	- If you have a video included in your vologram asset folder, then select **Use video in assets folder**.
 	- If you do not have a video file or you wish to overwrite the video file with another, you can drag and drop a VideoClip
object into the box marked “Drag Video Clip Here”.
 	- Alternatively, you can manually enter the location of a video file into the text field above the drag-and-drop box. Note
that the location is relative to the root of the project and not the Assets folder.
 	- If you are using embedded images, ensure the Video Texture text field is blank.
6. Under Audio Settings:
	- If you have a video included in your vologram asset folders that contains the audio of the vologram, you can leave this
field blank.
	- If you do not have a video file or you wish to overwrite the audio, you can drag and drop an AudioClip object into the
Volograms Audio field.
	- If not using audio, ensure the Volograms Audio is set to None.
7. Under Play Settings, adjust these as you see fit:
	- **Play on start**: the vologram will start to play immediately after preparing.
	- **Loop**: The vologram will loop back to the start after playback.
	- **Boomerang Loop**: Plays the vologram with a boomerang loop effect (only compatible with volograms with embedded
texture images).
	- **Material**: Change the material used for rendering the vologram.
	- **Buffer size**: How many frames are pre-loaded (recommended value is 5).
	- **Audio Delay**: How long should the audio wait (in seconds) from when the vologram starts playing to start playing.
	- **Start frame**: Which frame should the vologram start playing at.
	- **End frame**: Which frame should the vologram finish playing or loop at.
	- **Frame rate**: If there is no video texture, how fast should the vologram play (recommended value is 30, other values
may lower performance).
	- **Playback speed**: If there is a video texture, how fast should the vologram play (recommended value is 1, other values
may lower performance).
8. The Vologram Transformation Adjustment Settings do not need to be activated.
9. Under Editor Preview, the vologram can be previewed using the slider and preview control buttons.

### Playing Volograms, step by step (Version 1.5.01)

1. Create an Empty Object in your scene and name it *Vologram*, for example.
2. Create another Empty Object and make it the child of *Vologram*. Name this object Player.
3. Add the VologramsPlayer component to Player.
4. For the *Mesh Folder*, it is recommended to find the mesh folder using the "Open Volograms Asset Folder" button. This will open your file explorer and you can locate the mesh folder from there.
	- If a video file is detected in the opened folder, you will be asked if you want this video to be the texture of the vologram.
    - Alternatively, select from the dropdown whether the mesh folder is in the StreamingAssets folder or elsewhere. Then enter the folder location into the text box.
5. The VologramsPlayer inspector panel will indicate if the necessary files are found
6. For the *Video Texture*, it is recommended to use the "Open Vologram Video Texture" button to locate your .mp4 video texture file.
    - Alternatively, you can select which folder the file is in using the Path Type dropdown, and enter in the relative path to the file in the text box.
7. For the *Audio*, it is recommended to use the "Open Vologram Audio" button to locate your .mp3 audio file.

#### Next Settings
1. Under Play Settings, adjust these as you see fit:
	- **Play on start**: the vologram will start to play immediately after preparing.
	- **Loop**: The vologram will loop back to the start after playback.
	- **Boomerang Loop**: Plays the vologram with a boomerang loop effect (only compatible with volograms with embedded
texture images).
	- **Material**: Change the material used for rendering the vologram.
	- **Buffer size**: How many frames are pre-loaded (recommended value is 5).
	- **Audio Delay**: How long should the audio wait (in seconds) from when the vologram starts playing to start playing.
	- **Start frame**: Which frame should the vologram start playing at.
	- **End frame**: Which frame should the vologram finish playing or loop at.
	- **Frame rate**: If there is no video texture, how fast should the vologram play (recommended value is 30, other values
may lower performance).
	- **Playback speed**: If there is a video texture, how fast should the vologram play (recommended value is 1, other values
may lower performance).
2. The Vologram Transformation Adjustment Settings do not need to be activated.
3. Under Editor Preview, the vologram can be previewed using the slider and preview control buttons.
4. You can choose to hide/show the associated VideoPlayer and AudioSource components by selecting **Hide Video Player** and **Hide Audio Player**

## Reference
### VologramsPlayer reference (Version 1.5.00 and Before)
If you want to go further and play the Vologram from a Unity script you can use these functions to control playback:

| Properties | |
| --- | --- |
| `VologramsHeader VologramsPlayer.MetaHeader` | (`get` only) Returns a struct containing some meta-data about the vologram |
| `Material  VologramsPlayer.ActiveMeshMaterial` | (`set` only) Sets the vologram renderer material at runtime |
| `int VologramsPlayer.Frame` | (`get` only) Returns the number of the frame just rendered |

| Methods | |
| --- | --- |
| `void VologramsPlayer.Play()` | Begins or continues the vologram playback |
| `void VologramsPlayer.PlayDelayed(float time)` | Begins or continues the vologram playback after a specified time (in seconds) has passed |
| `void VologramsPlayer.Pause()` | Pauses the vologram playback. |
| `void VologramsPlayer.Stop()` | Stops the vologram playback and returns the video and audio players to the start. |
| `void VologramsPlayer.Restart()` | Stops the vologram playback and starts playback from the beginning. |
| `void VologramsPlayer.Seek(int frameNumber)` | Moves the player to the given frame |
| `bool VologramsPlayer.IsPlaying()` | Returns `true` if the vologram is playing, false otherwise. |
| `bool VologramsPlayer.IsReady()` | Returns `true` if the vologram is prepared to start playback, false otherwise. |
| `void VologramsPlayer.Clear()` | Clears the vologram assets. |
| `int VologramsPlayer.GetTotalNumberOfFrames()` | Gets the total number of frames in the vologram, i.e. it is unaffected by the start frame and end frame values. |
| `bool VologramsPlayer.IsMuted()` | Returns true is the vologram has audio and is muted. |
| `void VologramsPlayer.SwapVologramAssets(...)` | Resets the vologram with new assets. |


### VologramsPlayer.SwapVologramAssets() Method
This method enables users to switch the vologram's assets at runtime. The method has different parameters depending on the assets he user wants to swap (Note: in all methods, `useAbsolutePath` is `false` by default):

| Parameters | |
| --- | --- |
| `string assetFolder, string textureUrl, AudioClip newAudio, bool useAbsolutePath` | Changes all assets. The video clip is changed through it's url. |
| `string assetFolder, VideoClip texture, AudioClip newAudio, bool useAbsolutePath` | Changes all assets. The video clip is changed through an object. |
| `string assetFolder, string textureUrl, bool useAbsolutePath` | Changes the mesh and video assets. The video clip is changed through it's url. |
| `string assetFolder, VideoClip texture, bool useAbsolutePath` | Changes the mesh and video assets. The video clip is changed through an object |
| `string assetFolder, AudioClip newAudio, bool useAbsolutePath` | Changes the mesh and audio assets. Changes the video asset if one is detected in the asset folder. |
| `string assetFolder, bool useAbsolutePath` | Changes the mesh assets. Changes the video asset if one is detected in the asset folder. |

### VologramsPlayer reference (Version 1.5.01)
If you want to go further and play the Vologram from a Unity script you can use these functions to control playback:

| Properties | |
| --- | --- |
| `VologramPath AssetPath` | The path to the folder containing the .vols files, player must be stopped to set |
| `bool UseAbsolutePath` | Set to true if the vologram is stored locally but not in the StreamingAssets folder, player must be stopped to set |
| `int BufferSize` | The maximum number of frames that will be buffered during play, player must be stopped to set |
| `VologramPath VideoTextureUrl` | The video texture file, player must be stopped to set |
| `AudioClip VologramAudio` | The audio clip, player must be stopped to set |
| `float AudioStartTime` | The audio clip starting point (in seconds), player must be stopped to set |
| `int StartFrame` | Starting frame number of the vologram, player must be stopped to set |
| `int EndFrame` | Last frame number of the vologram, player must be stopped to set |
| `float PlaybackSpeed` | Speed at which the vologram plays at, player must be stopped to set |
| `int FPS` | Frame rate of the vologram (usually 30), player must be stopped to set |
| `bool PlayOnStart` | If true, the vologram will play immediately after preparing, player must be stopped to set |
| `bool IsLooping` | If true, the vologram will loop back to the start after finishing |
| `bool Muted` | If true, no audio will play from the vologram |
| `Material VologramsPlayer.MeshMaterial` | (`set` only) Sets the vologram renderer material at runtime |
| `VologramsHeader VologramsPlayer.Header` | (`get` only) Returns a struct containing some meta-data about the vologram |
| `int VologramsPlayer.Header.TotalFrameCount` | (`get` only) Number of frames in the vologram |
| `int VologramsPlayer.Frame` | (`get` only) Returns the number of the frame just rendered |

The `AssetPath` and `VideoTextureUrl` properties are a struct known as the the `VologramPath`. It contains 2 properties that helps the player find files across multiple platforms:

```
public struct VologramPath
{
    public PathType PathType;
    public string Path;
}
```

The `PathType` is an enum with the following members that refers to a path in Unity:

```
public enum PathType
{
    Absolute,	// Absolute path
    StreamingAssets,	// `Application.streamingAssetsPath`
    PersistentData	// `Application.persistentDataPath`
}
```

Here are the other properties and methods of the `VologramPath` struct:

| `VologramPath` Methods | |
| --- | --- |
| `VologramPath(PathType pathType, string path)` | Constructor for a new `VologramPath` |
| `static VologramPath InterpretFromPath(string path)` | Creates a `VologramPath` based on a full path |
| `string PathTypeAsString` | Returns the associated path of the `PathType` value |
| `string FullPath` | Returns the entire path by combining the `PathType` and `Path` |

Returning to the `VologramsPlayer`:

| Methods | |
| --- | --- |
| `void VologramsPlayer.Play()` | Begins or continues the vologram playback |
| `void VologramsPlayer.PlayDelayed(float time)` | Begins or continues the vologram playback after a specified time (in seconds) has passed |
| `void VologramsPlayer.Pause()` | Pauses the vologram playback. |
| `void VologramsPlayer.Stop()` | Stops the vologram playback and returns the video and audio players to the start. |
| `void VologramsPlayer.Restart()` | Stops the vologram playback and starts playback from the beginning. |
| `void VologramsPlayer.Seek(int frameNumber)` | Moves the player to the given frame |
| `bool VologramsPlayer.IsPlaying()` | Returns `true` if the vologram is playing, false otherwise. |

| Delegates & Events | |
| --- | --- |
| `delegate void VologramEventDelegate(VologramsPlayer player)` | Delegate that accepts a `VologramsPlayer` as input |
| `delegate void VologramFrameEventDelegate(VologramsPlayer player, long frameNumber)` | Delegate that accepts a `VologramsPlayer` and `long` number as input |
|   |   |
| `event VologramEventDelegate OnVologramPrepared` | Invoked when the `VologramsPlayer` finishes preparing |
| `event VologramEventDelegate OnVologramPlay` | Invoked when the `VologramsPlayer` begins playing |
| `event VologramEventDelegate OnVologramPause` | Invoked when the `VologramsPlayer` is paused |
| `event VologramEventDelegate OnVologramStop` | Invoked when the `VologramsPlayer` is stopped |
| `event VologramEventDelegate OnVologramLoop` | Invoked when the `VologramsPlayer` loops from the end back to the start |
| `event VologramFrameEventDelegate OnVologramSeekComplete` | Invoked when the `VologramsPlayer` finishes seeking a frame |

Example of use:
```
void OnEnable()
{
	player.OnVologramPlay += vologramsPlayer =>
	{
    	print($"Playing: {vologramsPlayer.AssetPath}");
	};

	player.OnVologramSeekComplete += (vologramsPlayer, frame) =>
	{
    	print($"Seeked frame {frame}: {vologramsPlayer.AssetPath}");
	};
}

// Alternatively, declare a function separately

private void PlayerOnVologramPause(VologramsPlayer vologramsPlayer)
{
    print($"Pausing: {vologramsPlayer.AssetPath}");
}

void OnEnable()
{
	player.OnVologramPause += PlayerOnVologramPause;
}

void OnDisable()
{
	player.OnVologramPause -= PlayerOnVologramPause;
}

```

## Get In Touch {#contact}
### Any other questions?
If you are still having trouble to play the assets or you get any errors, do not hesitate and get in touch with us. One of our developers will help you figure out what is going wrong. Send us an email to: [support@volograms.com](mailto:support@volograms.com) and we will get back to you
as soon as we can.

## Changelog {#changelog}

### Version 1.5.01 - May 11th 2021
- Removed the "Load from Folder" option in Inspector
- When opening mesh folder, if video file is detected a prompt will ask if that video should be used as the texture
- Added `VologramPath` struct to handle paths across different platforms

### Version 1.5.01 - December 4th 2020
- Rework of the Unity Editor Inspector
- Rework of the VologramPlayer script
- Added NLayer (Third-Party) scripts for loading MP3 files

### Version 1.5.00 - October 30th 2020
- Added the "Use video in assets folder" menu option
    - Solves bug of videoTextureUrl being set to the development machine path instead of build device path

### Version 1.5.00 - October 23th 2020
- Changes to how the editor behaviour works
    - There is no longer a second video texture player for the editor preview
    - Fixed some bugs with the playing the vologram in the editor
- Fixed bug where the vologram transform would reset when going into play mode
- Added `Seek()` function

### Version 1.4.04 Fix 2 - September 29th 2020
- Fixed issue where Unity would not clear previous data in the video player on Stop() or if current data contained error.
	- Instead of a VideoPlayer or AudioSource component being added to the object with the VologramsPlayer, a new hidden object is created in the scene which contains these media components.
- Fixed bug where sound from the video texture would not be muted with Mute().

### Version 1.4.04 Fix 1 - September 2020
- Removed the `VologramsToolkit.Scripts.Player` namespace for convenience.
- Changed the target material property of the VologramsPlayer texture player from `"_MainTex"` to `"_VMainTex"`.
	- This solves a bug where the texture from the video player is loaded into a material when it is not wanted.
	- The `VologramsUnlit` shader has been edited so that the base texture has the property `"_VMainTex"`.
	- When creating new shaders for volograms the main texture must have the property `"_VMainTex".`
- Added a new property that changes the Vologram material at runtime
	- Use `VologramsPlayer.meshMaterial` to change the material before the vologram starts playing.
	- Use `VologramsPlayer.ActiveMeshMaterial` to change the material at runtime.
- Updated README to include documentation and 1.4 references.
- Fixed bug with `VologramsPlayerEditor` script to account for absolute path when asset folder is dragged into the inspector.
- Fixed bugs in the `SwapVologramAssets` methods that would skip checking the asset folder for a video texture.
- Included the third-party software BetterStreamingAssets in the package.

## Third-Party Software {#third-party}
### [BetterStreamingAssets](https://github.com/gwiazdorrr/BetterStreamingAssets)
Better Streaming Assets is a plugin that lets you access Streaming Assets directly in an uniform and thread-safe way, with tiny overhead. Mostly beneficial for Android projects, where the alternatives are to use archaic and hugely inefficient WWW or embed data in Asset Bundles. API is based on Syste.IO.File and System.IO.Directory classes.

### [NLayer](https://github.com/naudio/NLayer)
NLayer is a fully managed MP3 to WAV decoder. The code was originally based on JavaLayer (v1.0.1), which has been ported to C#.
